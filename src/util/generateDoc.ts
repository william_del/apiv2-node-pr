const PDFDocument = require("pdfkit-table");
import path from "path";
import fs from "fs";
import pool from "../config/databasev2";
import * as INVOICE from "../models/querys/INVOICE";
import moment from "moment";

const fontBold = "Helvetica-Bold";
const font = "Helvetica";
const op1 = { continued: true, align: "justify" };
const op2 = { continued: true };
const op3 = { align: "justify" };
const op4 = { align: "left" };
const op5 = { continued: true, align: "justify", lineBreak: false };
const op6 = { align: "center", lineBreak: false };

const headerOpacity = 0.7;
const headerTableColor = "#F7C5C5";

const tablePVD = {
  headers: [
    {
      label: "Punto de venta",
      property: "name",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
    {
      label: "Valor",
      property: "price1",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
    {
      label: "Cantidad Trx",
      property: "price2",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
  ],
  rows: [],
};

const tablePVDByID = {
  headers: [
    {
      label: "Producto",
      property: "name",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
    {
      label: "Valor",
      property: "price1",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
    {
      label: "IVU",
      property: "price2",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
    {
      label: "Valor a pagar",
      property: "price2",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
    {
      label: "Descuento",
      property: "price2",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
    {
      label: "%",
      property: "price2",
      renderer: null,
      align: "center",
      headerColor: headerTableColor,
      headerOpacity,
    },
  ],
  rows: [],
};

export async function generateDoc(file: any, dispositivo: any, days: any) {
  //* Funciones para traer la data *//
  const infoPVDBD = INVOICE.infoPVD(dispositivo, days);
  let respInfo = (await pool.query(infoPVDBD)).rows;

  let arrayPVD: any = [];
  let arrayPUVEID: any = [];

  for (let i = 0; i < respInfo.length; i++) {
    if (i !== respInfo.length - 1) {
      arrayPUVEID.push({
        PUVE_ID: respInfo[i].PUVE_ID,
        nombre: respInfo[i].nombre,
      });
    }
    let newArray = [
      respInfo[i].nombre,
      `US$ ${respInfo[i].sum}`,
      respInfo[i].count,
    ];
    arrayPVD.push(newArray);
  }

  tablePVD.rows = arrayPVD;

  let doc = new PDFDocument({ size: "A4" });
  doc.fontSize(10);
  doc.pipe(file);
  doc = await header(doc, dispositivo, days);
  doc = await ventaPVD(doc, respInfo.length);

  for (let i = 0; i < arrayPUVEID.length; i++) {
    let data = INVOICE.infoPVDByID(arrayPUVEID[i].PUVE_ID, days);
    let respId = (await pool.query(data)).rows;
    let arrayPVDByID: any = [];
    for (let j = 0; j < respId.length; j++) {
      let newArray = [
        respId[j].PROD_DESCRIPCION,
        `US$ ${respId[j].TRAN_VALOR}`,
        `US$ ${respId[j].TRAN_IVU}`,
        `US$ ${respId[j].pagar}`,
        `US$ ${respId[j].descuento}`,
        respId[j].porcentaje,
      ];
      arrayPVDByID.push(newArray);
    }
    tablePVDByID.rows = arrayPVDByID;
    doc = await ventaPVDById(doc, arrayPUVEID[i].nombre);
  }

  doc = await footer(doc);

  doc.end();
  return doc;
}

async function header(doc: any, dispositivo: any, days: any) {
  let logo = fs.readFileSync(path.join(__dirname, "../temp/logo.png"));
  const fecha = moment()
    .subtract(Number(days), "days")
    .utcOffset(-5)
    .format("YYYY-MM-DD");

  /* Consulta en BD */
  const query = INVOICE.infoHeader(dispositivo);
  let response = await pool.query(query);

  doc
    .image(logo, 50, 50, { width: 120 })
    .fillColor("#000")
    .font(fontBold)
    .text("FACTURA N° ", 350, 50, { align: "center" })
    .font(fontBold)
    .text("Estado: ", 300, 65, op1)
    .font(font)
    .text("Activo", op3)
    .font(fontBold)
    .text("Periodo de corte: ", 300, 77, op1)
    .font(font)
    .text(`${fecha} al ${fecha}`, op3)
    .font(fontBold)
    .text("Nombre del comercio: ", 300, 89, op1)
    .font(font)
    .text(`${response.rows[0].nombre}`, op3)
    .font(fontBold)
    .text("Código cliente: ", op1)
    .font(font)
    .text(`${response.rows[0].COME_ID}`, op3);

  return doc;
}

async function ventaPVD(doc: any, respInfo: any) {
  doc
    .moveTo(25, 185)
    .lineTo(580, 185)
    .stroke()
    .font(fontBold)
    .fontSize(12)
    .text("VENTA POR PDV", 50, 200, { align: "center" });

  doc.moveDown();
  doc.moveDown();

  doc.table(tablePVD, {
    width: 450,
    x: 73,
    padding: 5,
    prepareHeader: () => doc.font(fontBold),
    prepareRow: (row: any, indexColumn: any, indexRow: any, rectRow: any) => {
      doc.font(font);
      indexRow === respInfo - 1 && doc.font(fontBold);
    },
  });

  return doc;
}

async function ventaPVDById(doc: any, nombre: any) {
  if (doc.y > 500) doc.addPage();
  else {
    doc.moveDown();
    doc.moveDown();
  }

  doc.font(fontBold).text(`* ${nombre}`, { align: "left" });
  doc.moveDown();
  doc.moveDown();
  doc.table(tablePVDByID, {
    width: 450,
    x: 73,
    prepareHeader: () => doc.font(fontBold),
    prepareRow: (row: any, indexColumn: any, indexRow: any, rectRow: any) => {
      doc.font(font);
    },
  });

  return doc;
}

async function footer(doc: any) {
  doc
    .moveTo(20, doc.page.height - 60)
    .lineTo(580, doc.page.height - 60)
    .stroke();

  doc.fontSize(7);

  doc.text("CONEXRED CARIBE, inc.", 20, doc.page.height - 45, op6);
  doc.text(
    "PO Box 1974, Trujillo Alto, P.R. 00977",
    20,
    doc.page.height - 35,
    op6
  );
  doc.text("Telefono: (787)302-3052", 20, doc.page.height - 25, op6);
  doc.text("www.pagatodopr.com", 20, doc.page.height - 15, op6);
  return doc;
}
