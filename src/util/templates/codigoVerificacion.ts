export function template_codigoVerificacion(terminal: string, fecha: string, msj: string, codigo: string): string {
    const sdf = new Intl.DateTimeFormat('es-ES', { year: 'numeric', month: 'long', day: 'numeric' });
    
    const htmlMessage = `
      <h1> Registro de Terminal </h1>
      <p>Terminal: ${terminal}</p>
      <p>${fecha}</p>
      <p>${msj}<span>${codigo}</span></p>
    `;
  
    return htmlMessage;
}