import pool from "../config/databasev2";
import * as USU from "../models/querys/USUARIO";
import * as TRX from "../models/querys/TRANSACCION";

export async function getPasswordMd5(puveId: any, dispId: any, password: any) {
    let response: any;
    let passmd5: any;
    try {
        const query = USU.getMd5Pass(dispId, password);
        response = await pool.query(query);
        if (response.rows.length > 0) {
            passmd5 = response.rows[0].passmd5;
        }else{
            passmd5 = 'not_found'
        }
        return passmd5;
    } catch (error) {
        return 'not_found'
    }
}

export async function getProductByCode(code: any) {
    let response: any;
    let originCode: any;
    try {
        const query = TRX.queryGetProductByCode(code);
        response = await pool.query(query);
        if (response.rows.length > 0) {
            originCode = response.rows[0].origincode;
        }else{
            return originCode = 'not_found';
        }
        return originCode;
    } catch (error) {
        return 'not_found'
    }
}