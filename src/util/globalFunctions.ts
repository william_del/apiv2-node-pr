import nodemailer from 'nodemailer';
import mandrill from '@mailchimp/mailchimp_transactional';

export async function sendEmailOld(to: string, subject: string, htmlBody: string) {
    try {
        const transporter = nodemailer.createTransport({
            host: process.env.EMAIL_SERVER || '',
            port: process.env.EMAIL_PORT || '',
            secure: false,
            requireTLS: true,
            auth: {
                user: process.env.EMAIL_U || '',
                pass: process.env.EMAIL_P || '',
            },
            tls: {
                ciphers: "SSLv3",
            },
        });

        const mailOptions = {
            from: process.env.EMAIL_U || '',
            to: to,
            subject: subject,
            html: htmlBody,
        };

    const res = await transporter.sendMail(mailOptions);
    console.log("🚀 ~ file: functions.ts:153 ~ Functions ~ .then ~ res:", res);

  } catch (err) {
    console.log("sendEmail false");
    console.log(err);

  }
}

export function generarCodigo(): string {
    const caracteresPermitidos = process.env.CADENA_CODIGO || ' ';
    let codigo = '';
  
    for (let i = 0; i < 8; i++) {
      const indiceAleatorio = Math.floor(Math.random() * caracteresPermitidos.length);
      codigo += caracteresPermitidos.charAt(indiceAleatorio);
    }
  
    return codigo;
}

export async function sendEmail(to: string, subject: string, htmlBody: string) {
    try {
        const mandrillClient = mandrill(process.env.MANDRILL_KEY || '');

        const subjectPrefix = process.env.NODE_ENV === 'develop' ? '[TEST] ' : '';
        const finalSubject = subjectPrefix + subject;

        const message = {
            from_email: process.env.MANDRILL_FROM || '',
            to: [{ email: to }],
            subject: finalSubject,
            html: htmlBody,
        };

        const response = await mandrillClient.messages.send({ message: message });
        console.log("Email sent successfully:", response);
    } catch (err) {
        console.error("Failed to send email:", err);
    }
}



