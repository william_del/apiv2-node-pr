export class HttpResponse {
  status: boolean;
  message: string;
  data: object;

  constructor() {
    this.status = false;
    this.message = "";
    this.data = {};
  }

  create(nameEntity: string, dataCreated: object) {
    this.status = true;
    this.message = `${nameEntity} created successfully`;
    this.data = dataCreated;
  }

  createMany(nameEntity: string, dataCreated: []) {
    this.status = true;
    this.message = `${nameEntity} created successfully`;
    this.data = dataCreated;
  }

  update(statusEntry: Boolean,messageEntry: string) {
    this.status = true;
    //this.message = `Updated successfully`;
    this.message = messageEntry;
    this.data = {};
  }

  emailVerified(email: string) {
    this.status = true;
    this.message = `The email ${email} has been verified successfully`;
  }

  delete(nameEntity: string, dataCreated: object) {
    this.status = true;
    this.message = `${nameEntity} deleted successfully`;
    this.data = dataCreated;
  }

  findOne(dataFinded: object) {
    this.status = true;
    this.message = `Registro encontrado`;
    this.data = dataFinded;
  }

  findAll(allRecords: any) {
    this.status = true;
    this.message = `Todos los registros encontrados`;
    this.data = allRecords;
  }

  accessDenied() {
    this.message = `Acceso denegado`;
    this.data = { status: 401 };
  }

  findOnesByQuery(queryRecords: any) {
    this.status = true;
    this.message = `Data of your search`;
    this.data = queryRecords;
  }

  saveTempFile(url: any) {
    this.status = true;
    this.message = `Temporary location for your file`;
    this.data = url;
  }

  emptyRecords() {
    this.status = true;
    this.message = "No records found";
    this.data = [];
  }

  notFileSend() {
    this.message = "File not send";
    this.data = [];
  }

  errorEntityDuplicated(nameEntity: string, id: any) {
    this.message = `${nameEntity} with ID ${id} is already in our database`;
  }

  errorDuplicated() {
    this.message = "This record is already in our database";
  }

  errorFieldDuplicated(recordDuplicated: string, id: any) {
    this.message = `This ${recordDuplicated} with field ${id} is already in our database`;
  }

  errorEmptyObject(dataReceived: object) {
    this.message = `Data received to request is empty`;
    this.data = dataReceived;
  }

  error(message: any) {
    this.message = message;
    this.data = {};
  }

  success(message: string, data: object) {
    this.status = true;
    this.message = message;
    this.data = data ;
  }

  notSuccess(message: string, data: object) {
    this.status = false;
    this.message = message;
    this.data = { data };
  }

  successMsg(message: string) {
    this.status = true;
    this.message = message;
  }

  errorS3(message: string, dataReceived: object) {
    this.message = message;
    this.data = dataReceived;
  }

  errorFormatInvalid(numberID: any) {
    this.message = `ID received { id: ${numberID} }  is not number valid, check if is not empty or another type (string, null, undefined) etc`;
    // return this.message;
  }

  errorNotFoundID(nameEntity: string, id: any) {
    this.message = `${nameEntity} with ID ${id} was not found`;
  }

  errorNotRecordFound(nameEntity: string, field: string, value: any) {
    this.message = `Registro para ${nameEntity} con ${field} ${value} no fue encontrado`;
  }

  errorWithEmail(email: string, thing: string) {
    this.message = `The email ${email} has been removed from our database or is ${thing} verified`;
  }

  errorEmailDuplicate(email: string) {
    this.message = `The email ${email} is already registered, please login`;
  }

  errorNotClientFound(nameEntity: string, field: string, value: any) {
    //this.message = `${nameEntity} with field ${field} ${value} is not a client`;
    this.message = `User ${value} or password is not valid`;
  }

  operatorInactive(opeName: string) {
    //this.message = `${nameEntity} with field ${field} ${value} is not a client`;
    this.message = `${opeName} is inactive`;
  }

  errorCredencials() {
    this.message = `Credenciales incorrectas`;
  }

  personalized(status: any, message: any, dat: any) {
    this.status = status;
    this.message = message;
  }

  genericMsgResponse(msg: string) {
    //this.message = `${nameEntity} with field ${field} ${value} is not a client`;
    this.message = `${msg}`;
  }
}
