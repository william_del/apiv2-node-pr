const crypto = require('crypto');

const algorithm = 'aes-256-cbc';
const keyString = 'vObbGa4xZ5kQxqessvyOBf0EPGGIaRi+';
const key = crypto.createHash('sha256').update(String(keyString)).digest('base64').substr(0, 32);

export function decryptString(text: any) {
    let textParts = text.split('-');
    let iv = Buffer.from(textParts.shift(), 'hex');
    let encryptedText = Buffer.from(textParts.join('-'), 'hex');
    let decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}