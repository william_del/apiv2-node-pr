import * as fs from 'fs';
import * as path from 'path';

class BlacklistService {
    private blacklistPath = path.join(process.cwd(), 'blacklistips.json');
    private failedAttempts = new Map<string, number>();

    loadBlacklist() {
        if (!fs.existsSync(this.blacklistPath)) {
            fs.writeFileSync(this.blacklistPath, JSON.stringify([]));
        }
    }

    isBlacklisted(ip: string): boolean {        
        const blacklist = JSON.parse(fs.readFileSync(this.blacklistPath, 'utf8'));
        return blacklist.includes(ip);
    }

    recordFailedAttempt(ip: string) {
        //console.log("🚀 ~ BlacklistService ~ recordFailedAttempt ~ ip:", ip)
        const attempts = this.failedAttempts.get(ip) || 0;
        this.failedAttempts.set(ip, attempts + 1);
        console.log("🚀 ~ BlacklistService ~ recordFailedAttempt ~ attempts:", attempts)
        
        if (attempts + 1 >= 3) {
            this.addToBlacklist(ip);
        }
    }
        

    private addToBlacklist(ip: string) {
        const blacklist = JSON.parse(fs.readFileSync(this.blacklistPath, 'utf8'));
        if (!blacklist.includes(ip)) {
            blacklist.push(ip);
            fs.writeFileSync(this.blacklistPath, JSON.stringify(blacklist));
            this.failedAttempts.delete(ip);
        }
    }
}

const blacklistService = new BlacklistService();
export default blacklistService;