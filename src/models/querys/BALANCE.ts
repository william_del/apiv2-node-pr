
export const recBalance = (come:any, valor:any, pasarela:any, app:any, idunico:any, estado:any) => {
    return `select registra_cargarbalance('${valor}','${come}','${pasarela}','${app}','${idunico}','${estado}');`;
}

/*export const recBalanceWebHook = (come:any, valor:any, pasarela:any) => {
    return `select registra_cargarbalance_webhook('${valor}','${come}','${pasarela}');`;
}*/

export const traBalance = (valor:any, celular:any, comeEnvia:any, pasarela:any) => {
    return `select registra_transferirbalance('${valor}','${celular}','${comeEnvia}',${pasarela});`;
}

export const recBalancePreview = (come:any, valor:any) => {
    return `select registra_cargarbalance_preview('${come}','${valor}') as "idtrxwebhook";`;
}

/*export const recBalancePreviewTest = () => {
    return `select "" as idtrxwebhook;`;
}*/

export const query_get_trxbalance = (unique: any) => {
    return `select "TRBA_ID"
                    ,"STATUS"
                    ,"VALOR"
                    ,"COME_ID"
            from "TRANSACCION_BALANCE" 
            where   "UNIQUE_ID" = '${unique}'
                    and "STATUS" = 0;`;
};

export const query_update_trxbalance = (unique: any, trxAth: any, statusAth: any) => {
    return `UPDATE "TRANSACCION_BALANCE" 
            SET "TRAN_ID_PASARELA" = '${trxAth}'
                ,"STATUS" = 2
                ,"STATUS_PASARELA" = '${statusAth}'
            where   "UNIQUE_ID" = '${unique}';`;
};