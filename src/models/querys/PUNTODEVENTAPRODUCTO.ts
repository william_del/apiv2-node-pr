// export const infoProductos = (puveId: any, puveCod: any) => {
//   return `select p4."PAIS_ID"
//                 ,p4."PAIS_NOMBRE"
//                 ,p3."PROV_ID"
//                 ,p3."PROV_NOMBRE"
//                 ,p2."PROD_ID"
//                 ,p2."PROD_CODIGO"
//                 ,p2."PROD_DESCRIPCION"
//                 ,p2."PROD_DESCRIPCIONTOTAL"
//             from "PUNTODEVENTAPRODUCTO" P
//             inner join "PRODUCTO" p2 on p."PROD_ID" = p2."PROD_ID"
//             inner join "PROVEEDOR" p3 on p3."PROV_ID" = p2."PROV_ID"
//             inner join "PAIS" p4 on p4."PAIS_ID" = p2."PAIS_ID"
//             inner join "PUNTODEVENTA" p5 on p5."PUVE_ID" = p."PUVE_ID"
//             where p."PUVE_ID" = ${puveId}
//                 and p5."PUVE_CODIGO" = ${puveCod}
//             order by p4."PAIS_NOMBRE", p3."PROV_NOMBRE" asc
//             ;`;
// };

export const infoProductos = (
  puveId: any,
  puveCod: any,
  paisId: any,
  tipoProducto: any,
  tipoTrx: any
) => {
  return `select	p4."PAIS_ID"
            ,p4."PAIS_NOMBRE" 
            ,p2."PROD_ID" 
            ,p2."PROD_CODIGO" 
            ,p2."PROD_CODIGOORIGEN" 
            ,p2."TIPR_ID" 
            , (select url from links.logoproducto where prodsplit = split_part(p2."PROD_DESCRIPCION", ' ', 1))  AS "PROD_SPLIT"
            ,p2."PROD_DESCRIPCION" 
            ,p2."PROD_DESCRIPCIONTOTAL" 
            ,p2."PROD_VALOR" 
            ,p2."PROD_MINIMO" 
            ,p2."PROD_MAXIMO" 
            ,p2."PROD_FEE" 
            ,case when SQ."CANTIDAD" is null then '0' else SQ."CANTIDAD" end "TOPFIVE"
            ,p2."PROV_ID"
            from "PUNTODEVENTAPRODUCTO" P
            inner join "PRODUCTO" p2 on p."PROD_ID" = p2."PROD_ID" 
            inner join "PAIS" p4 on p4."PAIS_ID" = p2."PAIS_ID"  
            inner join "PUNTODEVENTA" p5 on p5."PUVE_ID" = p."PUVE_ID"
            left join (select	"PROD_ID" as "PRODID"
                          ,COUNT(*) as "CANTIDAD"
                      from "TRANSACCION"
                      where "PUVE_ID" = ${puveId}
                          and "TRAN_FECHA" between current_date-5 and current_date 
                          and "TRAN_TIPO" = '${tipoTrx}'
                      group by 1
                      order by 2 desc 
                      limit 5) SQ on SQ."PRODID" = p2."PROD_ID" 
            where p."PUVE_ID" = ${puveId}
                and p5."PUVE_CODIGO" = ${puveCod}
                ${paisId === "-1" ? `` : `and p4."PAIS_ID" = ${paisId}`}
                and p2."TIPR_ID" = ${tipoProducto}
                and p."PTOP_ESTADO" = 1
                and p2."PROD_ACTIVO" = 1
                and p2."PROD_ID" not in (479,477,667,478,3862)
            order by p4."PAIS_NOMBRE",p2."PROD_DESCRIPCION" asc
            ;`;
};

export const infoProductosOldVersion = (
  puveId: any,
  locationId: any,
  tipoProducto: any,
  paisId: any
) => {
  console.log(puveId, locationId, tipoProducto, paisId);
  return `select	p4."PAIS_ID"
            ,p4."PAIS_NOMBRE" 
            ,p2."PROD_ID" 
            ,p2."PROD_CODIGO" 
            ,p2."TIPR_ID" 
            ,p2."PROD_DESCRIPCION" 
            ,p2."PROD_DESCRIPCIONTOTAL" 
            ,p2."PROD_VALOR" 
            ,p2."PROD_MINIMO" 
            ,p2."PROD_MAXIMO" 
            ,p2."PROD_FEE" 
            ,p2."PROD_LENGTH"
            ,p2."PROV_ID" 
            ,p2."TISP_ID" 
            from "PUNTODEVENTAPRODUCTO" P
            inner join "PRODUCTO" p2 on p."PROD_ID" = p2."PROD_ID" 
            inner join "PAIS" p4 on p4."PAIS_ID" = p2."PAIS_ID"  
            inner join "PUNTODEVENTA" p5 on p5."PUVE_ID" = p."PUVE_ID"
            where p."PUVE_ID" = ${puveId}
                and p5."PUVE_CODIGO" = ${locationId}
                ${paisId ? `and p4."PAIS_ID" = ${paisId}` : ``}
                and p2."TIPR_ID" = ${tipoProducto}
                and p."PTOP_ESTADO" = 1
                and p2."PROD_ACTIVO" = 1
            order by p4."PAIS_NOMBRE",p2."PROD_DESCRIPCION" asc
            ;`;
};
export const infoPaises = (puveId: any, puveCod: any, tipoProducto: any) => {
  return `select    p4."PAIS_ID"
            ,p4."PAIS_NOMBRE"         
            from "PUNTODEVENTAPRODUCTO" P
            inner join "PRODUCTO" p2 on p."PROD_ID" = p2."PROD_ID" 
            inner join "PAIS" p4 on p4."PAIS_ID" = p2."PAIS_ID"  
            inner join "PUNTODEVENTA" p5 on p5."PUVE_ID" = p."PUVE_ID"
            where p."PUVE_ID" = ${puveId}
                and p5."PUVE_CODIGO" = ${puveCod}
                and p2."TIPR_ID" = ${tipoProducto}
                and p."PTOP_ESTADO" = 1
                and p2."PROD_ACTIVO" = 1
            group by p4."PAIS_ID"
                ,p4."PAIS_NOMBRE" 
            order by p4."PAIS_NOMBRE" asc;`;
};

export const getMovimientos = (puveId: any, fechaIni: any, fechaFin: any) => {
  return `select	numero
                  ,id
                  ,fecha
                  ,valor
                  ,estado
                  ,operador
                  ,"TD"."TIDI_DESCRIPCION" AS "trantipo"
                  ,usuario as nombres
                  ,tipo 
          from transaccionrecargaspines AS "A" 
          inner join "DISPOSITIVOS" AS "D" ON "D"."DISP_ID" = "A".dispid 
          inner join "TIPODISPOSITIVO" AS "TD" ON "TD"."TIDI_ID" = "D"."TIDI_ID" 
          where pdv = ${puveId}
          and to_char(fecha, 'YYYY-MM-DD') between '${fechaIni}' and '${fechaFin}'
          and operador not like 'FEE%' ;`;
};

export const getTopFive = (comeId: any) => {
  return `select	p."PROD_ID"
                  ,p."PROD_CODIGO"
                  ,p."PROD_DESCRIPCION"
          from "PRODUCTO" p 
          inner join (select	"PROD_ID" as "PRODID"
                          ,COUNT(*)
                      from "TRANSACCION"
                      where "COME_ID" = ${comeId}
                          and "TRAN_FECHA" between date_trunc('month', current_date) and current_date 
                          and "TRAN_TIPO" = '04'
                      group by 1
                      order by 2 desc 
                      limit 5) t5 on t5."PRODID" = p."PROD_ID" 
          where p."PROD_ID" not in (479,477,667,478,3862);`;
};

export const q_getCountry = () => {
  return `select "PAIS_ID" as "Id",
          "PAIS_NOMBRE" as "Pais",
          "PAIS_INDICATIVO" as "Indicativo"
        from "PAIS" ;;`;
};