export const q_getMenus = (puveCodigo:any, reportonly:any) => {

    let query;
    if(reportonly==1){
        query = ` PV."PUVE_CODIGO" = '${puveCodigo}' and M.id=4` ;
    }else{
        query = ` PV."PUVE_CODIGO" = '${puveCodigo}'` ;
    };


    return `SELECT DISTINCT
                M.parent AS parent,
                M.id AS "MENU_ID",
                M.description AS "MENU_DESCRIPCION",
                M.type_menu AS type_menu,
                M.components_id AS components,
                C.url AS component_url,
                C.description AS component_description,
                M.type_products_id AS "MENU_TIPO_PROD",
                M.icon AS icon,
                M.state AS state,
                M.order_item AS "MENU_ORDEN",
                M.class_color AS color ,
                M.svg_icon           as "MENU_ICON",
                M.route              as "MENU_PATH"
            FROM 
                portal.menus M,
                portal.puntodeventaproducto_menus PVP,
                portal.producto_menus P,
                portal.puntodeventa_menus PV,
                portal.components C
            WHERE 
                (M.type_products_id = P."TIPR_ID" OR M.type_products_id IS NULL) AND
                M.components_id = C.id AND
                PVP."PROD_ID" = P."PROD_ID" AND
                PV."PUVE_ID" = PVP."PUVE_ID" AND
                M.state = 1 AND
                PVP."PTOP_ESTADO" = 1 AND
                P."PROD_ACTIVO" = 1 AND
                P."PROD_ESTADO" = 1 AND
                ${query}
            order by M.order_item;`;
};

export const queryGetBanner = (flag: Number) => {

    let query;
    if(flag==1){
        query = `state = '${flag}'` ;
    }
    if(flag==2){
        query = `state = 2 and priority = 0` ;
    }
    if(flag==3){
        query = `state = 2 and priority = 1` ;
    }

    return `select	id					as "BANNER_ID"
                    ,base_64_data		as "BANNER_64"
            from portal.publicities
            where ${query}
            order by 1;`;
};

export const q_getReportOnly = (usua:any) => {
    return `select  count(*)
            from "USUARIO" 
            where   "USUA_ID" = ${usua} 
                    and "USUA_REPORTONLY"=1;`;
};

export const queryGetNotification = () => {
    return `select id,titulo,texto,estado from portal.notificaciones where estado = '1';`;
};

