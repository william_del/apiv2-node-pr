export const infoHeader = (dispositivo: any) => {
  return `select	i."INPE_PRIMERNOMBRE" || ' ' || i."INPE_PRIMERAPELLIDO" || ' (' || i."INPE_RAZONSOCIAL" || ')' as nombre
                ,c."COME_ID" 
            from "COMERCIO" c
            inner join "INFORMACIONGENERAL" i on i."ENTI_ID" = c."COME_ID" and i."INPE_PRINCIPAL" =1
            where c."COME_ID" =  ${dispositivo};`;
};

export const infoPVD = (dispositivo: any, days: any) => {
  return `select t."PUVE_ID" 
                ,i."INPE_RAZONSOCIAL" as nombre
                ,sum(t."TRAN_VALOR"::FLOAT)
                ,count(*)
            from "TRANSACCION" t
            inner join "PUNTODEVENTA" p on p."PUVE_ID" = t."PUVE_ID" 
            inner join "INFORMACIONGENERAL" i on i."ENTI_ID"  = p."PUVE_ID" and "INPE_PRINCIPAL"= 1
            where t."COME_ID" = ${dispositivo}
                and t."TRAN_FECHA" = current_date - ${days}
                and t."TRAN_ESTADO" = 1
            group by 1,2
            union all
            select	t."COME_ID"
                ,'TOTAL'
                ,sum(t."TRAN_VALOR"::FLOAT)
                ,count(*)
            from "TRANSACCION" t
            inner join "PUNTODEVENTA" p on p."PUVE_ID" = t."PUVE_ID" 
            inner join "INFORMACIONGENERAL" i on i."ENTI_ID"  = p."PUVE_ID" and "INPE_PRINCIPAL"= 1
            where t."COME_ID" = ${dispositivo}
                and t."TRAN_FECHA" = current_date - ${days}
                and t."TRAN_ESTADO" = 1
            group by 1,2
            ;`;
};

export const infoPVDByID = (id: any, days: any) => {
  return `select	p."PROD_DESCRIPCION" 
                    ,t."TRAN_VALOR" 
                    ,t."TRAN_IVU" 
                    ,sum((t."TRAN_VALOR"::FLOAT)-((t."TRAN_VALOR"::FLOAT*c2."COMI_MARGEN")/100)) as pagar
                    ,sum((t."TRAN_VALOR"::FLOAT*c2."COMI_MARGEN")/100) as descuento
                    ,round(c2."COMI_MARGEN",2) as porcentaje
            from "TRANSACCION" t 
            inner join "PRODUCTO" p on p."PROD_ID" = t."PROD_ID"
            inner join "COMERCIO" c on c."COME_ID" = t."COME_ID"
            inner join "COMISION" c2 on c2."ENTI_ID" = c."COME_ID" and c2."PROD_ID" = t."PROD_ID" 
            where t."PUVE_ID" = ${id}
                and t."TRAN_FECHA" = current_date - ${days}
            group by 1,2,3,6
;`;
};
