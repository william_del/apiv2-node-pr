export const q_statusTrxWithIdExtern = (dispId: any, numero: any, valor: any, transaccion: any) => {
    return `select	t."TRAN_ID" as idtransaccion
                ,t2."TRRE_RESPUESTAOPERADOR" as rtaoperador
                ,t2."TRRE_TRANSACCION" as idoperador
                ,t."TRAN_FECHA" as fechatransaccion
            FROM "TRANSACCION" t 
            --inner join "PUNTODEVENTA" p on p."PUVE_ID" = t."PUVE_ID" 
            --inner join "DISPOSITIVOS" d on d."PUVE_ID" = p."PUVE_ID" and d."PUVE_ID" = t."PUVE_ID" 
            inner join "TRANSACCIONRECARGA" t2 on t2."TRAN_ID" = t."TRAN_ID" 
            where t."TRAN_TRACE"::BIGINT = '${transaccion}'
                and t2."TRRE_NUMERO" = '${numero}'
                and t."DISP_ID" = ${dispId}
                and t."TRAN_VALOR" = '${valor}'
            ;`;
};

export const q_transactionstatus = (type: any, code: any, disp: any, puveid: any, fechaIni: any, fechaFin: any) => {
        //let query;
        let tranid,phone,fechaini,fechafin;
        fechaini='2023-01-01'
        if(type==1){
                //query = ` t2."TRRE_NUMERO" = '${code}'` ;
                tranid = 0;
                phone = code;
        }else{
                //query = ` t."TRAN_ID"::BIGINT = '${code}'` ;
                tranid = code;
                phone = '';
        };
        return `SELECT * from public.estadotransaccion_teltrx(${puveid}, ${tranid}, '${phone}', '${fechaIni}', '${fechaFin}');`;
        /*return `select	t."TRAN_ID" as idtransaccion
                        ,t."TRAN_FECHA" as fechatransaccion
                        ,t."TRAN_VALOR" as valor
                        ,p."PROD_DESCRIPCION" as producto
                        ,t3."TIPR_DESCRIPCION" as tipoproducto
                        
                        ,case when t2."TRRE_RESPUESTAOPERADOR" != '00' then 'EXITOSA' else 'FALLIDA' end  as rtaoperador
                        ,t2."TRRE_TRANSACCION" as traceoperador
                        ,'' as cajero
                FROM "TRANSACCION" t 
                inner join "TRANSACCIONRECARGA" t2 on t2."TRAN_ID" = t."TRAN_ID" 
                inner join "PRODUCTO" p on p."PROD_ID" = t."PROD_ID" 
                inner join "TIPOPRODUCTO" t3 on t3."TIPR_ID" = p."TIPR_ID" 
                where ${query} 
                        and t."DISP_ID" = ${disp};`;*/
    };

export const queryGetReports = () => {
    return `select  id 				as idReport,
                    description		as descReport,
                    jsonfields		as fieldsReport,
                    labels			as LabelsReport
            from portal.reporttypes
            where state = 1
            order by 1;`;
};

export const queryDetailReport = (idRep: Number
                                ,pdv: Number
                                ,dateInitial: any
                                ,finalDate: any) => {
                                console.log("🚀 ~ file: REPORTES.ts:28 ~ idRep", idRep)
                                console.log("🚀 ~ file: REPORTES.ts:31 ~ finalDate", finalDate)
                                console.log("🚀 ~ file: REPORTES.ts:31 ~ dateInitial", dateInitial)
                                console.log("🚀 ~ file: REPORTES.ts:31 ~ pdv", pdv)

    if (idRep==1){
        return `select 	numero
                        ,id
                        ,to_char(fecha, 'YYYY-MM-DD HH:MI:SS') AS fecha
                        ,valor
                        ,case when estado = 1 then 'EXITOSA' else 'RECHAZADA' end as estado
                        ,operador
                        ,"TD"."TIDI_DESCRIPCION" AS "trantipo"
                        ,usuario as nombres
                        ,tipo 
                from transaccionrecargaspines AS "A" 
                inner join "DISPOSITIVOS" AS "D" ON "D"."DISP_ID" = "A".dispid 
                inner join "TIPODISPOSITIVO" AS "TD" ON "TD"."TIDI_ID" = "D"."TIDI_ID" 
                where	pdv = ${pdv}
                        and to_char(fecha, 'YYYY-MM-DD') between '${dateInitial}' and '${finalDate}'
                        and operador not like 'FEE%' 
                order by 3 desc;`
    }
    if (idRep==4){
        return `select	A."DISP_ID" as dispid
                        , SUM("TRAN_VALOR"::NUMERIC(20,2)) as valor
                        ,SUM("MOVI_VALOR"::NUMERIC(20,2)) as comision
                        ,(SUM("TRAN_VALOR"::NUMERIC(20,2))-SUM("MOVI_VALOR"::NUMERIC(20,2))) as tranvalor
                        , TO_CHAR("TRAN_FECHAREGISTRO", 'YYYY-MM-DD') AS fecha
                FROM "TRANSACCIONDIA" A, "MOVIMIENTO_DIA" B
                where	A."TRAN_ESTADO"='1'
                        AND A."TRAN_ID"=B."TRAN_ID" AND B."TIMO_ID"=3 
                        AND "PUVE_ID" = ${pdv}
                        and TO_CHAR("TRAN_FECHAREGISTRO", 'YYYY-MM-DD') between '${dateInitial}' and '${finalDate}'
                        AND "PROD_ID" NOT IN (477,478,479)
                GROUP BY dispid, fecha
                ORDER BY fecha DESC;`
    }
    if (idRep==5){
        return `select "PROD_DESCRIPCION" as producto
                        , SUM("TRAN_VALOR"::numeric(20,2)) as tranvalor
                        ,CASE when "TICO_ID"=1 
                            THEN TO_CHAR(A."MOVI_COMISION"*100,'999D99 "%"')
                            else TO_CHAR(A."MOVI_COMISION",'"US$"999D99') END as numero, SUM("MOVI_VALOR"::numeric(20,2)) as valor
                        ,SUM("TRAN_VALOR"::numeric(20,2))-SUM("MOVI_VALOR"::numeric(20,2)) as comision
                from "MOVIMIENTO_DIA" A, "TRANSACCIONDIA" B, "PRODUCTO" C
                where	A."TRAN_ID"=B."TRAN_ID" AND A."TIMO_ID"=3 
                        AND C."PROD_ID"=B."PROD_ID" 
                        AND "PUVE_ID" = ${pdv}
                        and "TRAN_ESTADO"='1' 
                        AND "TRAN_FECHA" between '${dateInitial}' and '${finalDate}'
                        AND B."PROD_ID" NOT IN (477,478,479)
                GROUP BY "PROD_DESCRIPCION"
                        , numero
                        , "TICO_ID"`
    }
    if (idRep==7){
        return `select	to_char("TRA"."TRAN_FECHAREGISTRO", 'YYYY-MM-DD HH:MI:SS') AS "fecha"
                        ,"TRA"."PUVE_ID" as "id"
                        ,"INF"."INPE_PRIMERNOMBRE" || ' ' || "INF"."INPE_PRIMERAPELLIDO" as "nombres"
                        ,"INF"."INPE_RAZONSOCIAL" as "razonsocial"
                        ,COUNT(*) as "cantidad"
                        ,sum("TRA"."TRAN_VALOR"::DECIMAL(10,2)) as "valor"
                        , SUM("MOV"."MOVI_VALOR"::NUMERIC(20,2)) as "comision"
                        , (sum("TRA"."TRAN_VALOR"::DECIMAL(10,2)) - SUM("MOV"."MOVI_VALOR"::NUMERIC(20,2))) as "tranvalor" 
                from "PUNTODEVENTA" as "PTO" 
                inner join "TRANSACCION" as "TRA" on "TRA"."COME_ID" = "PTO"."COME_ID" 
                inner join "INFORMACIONGENERAL" as "INF" on "TRA"."PUVE_ID" = "INF"."ENTI_ID" 
                inner join "MOVIMIENTO_DIA" as "MOV" on "MOV"."TRAN_ID" = "TRA"."TRAN_ID" AND "MOV"."TIMO_ID"=3 
                where	"INF"."INPE_PRINCIPAL"=1 
                        and "TRA"."TRAN_ESTADO"=1 
                        and "PTO"."PUVE_ID"= ${pdv}
                        and "TRA"."TRAN_FECHA" between '${dateInitial}' and '${finalDate}'
                        AND "TRA"."PROD_ID" NOT IN (477,478,479)  
                group by	"fecha"
                            ,"id"
                            ,"nombres"
                            ,"razonsocial" 
                order by "razonsocial","fecha"`
    }
    if (idRep==8){
        return `select "TRA"."PUVE_ID" as "id"
                        ,"INF"."INPE_PRIMERNOMBRE" || ' ' || "INF"."INPE_PRIMERAPELLIDO" as "nombres"
                        ,"INF"."INPE_RAZONSOCIAL" as "razonsocial"
                        ,COUNT(*) as "cantidad"
                        ,sum("TRA"."TRAN_VALOR"::DECIMAL(10,2)) as "valor"
                        , SUM("MOV"."MOVI_VALOR"::NUMERIC(20,2)) as "comision"
                        , (sum("TRA"."TRAN_VALOR"::DECIMAL(10,2)) - SUM("MOV"."MOVI_VALOR"::NUMERIC(20,2))) as "tranvalor" 
                from "PUNTODEVENTA" as "PTO" 
                inner join "TRANSACCION" as "TRA" on "TRA"."COME_ID" = "PTO"."COME_ID" 
                inner join "INFORMACIONGENERAL" as "INF" on "TRA"."PUVE_ID" = "INF"."ENTI_ID" 
                inner join "MOVIMIENTO_DIA" as "MOV" on "MOV"."TRAN_ID" = "TRA"."TRAN_ID" AND "MOV"."TIMO_ID"=3 
                where	"INF"."INPE_PRINCIPAL"=1 
                        and "TRA"."TRAN_ESTADO"=1 
                        and "PTO"."PUVE_ID"= ${pdv}
                        and "TRA"."TRAN_FECHA" between '${dateInitial}' and '${finalDate}'
                        AND "TRA"."PROD_ID" NOT IN (477,478,479)  
                group by	"id"
                            ,"nombres"
                            ,"razonsocial" 
                order by "razonsocial"`
    }
    if (idRep==9){
        return `select 	
                        id
                        ,to_char(fecha, 'YYYY-MM-DD HH:MI:SS') AS fecha
                        ,operador
                        ,aprobacion valororiginal
                        ,valor as porcentajepagatodo
                        
                from transaccionrecargaspines AS "A" 
                where	pdv = ${pdv}
                        and to_char(fecha, 'YYYY-MM-DD') between '${dateInitial}' and '${finalDate}'
                        and operador like 'FEE%' 
                order by fecha desc;`
    }
};
