  export const queryTipoDisp = (dispositivo: any) => {
    return `select "TIDI_ID" as tipodisp from "DISPOSITIVOS" where "DISP_ID" = ${dispositivo};`;
  };

export const login = (clave: string, dispositivo: any) => {
  return `select i."INPE_PRIMERNOMBRE" 
                ,i."INPE_PRIMERAPELLIDO"
                ,d."DISP_ID" 
                ,p."PUVE_CODIGO"
                ,p."PUVE_ID"
                ,p."COME_ID"
                ,case when (select "DISP_ID" from "DISPOSITIVOS" where "PUVE_ID" = p."PUVE_ID" and "DISP_ESTADO" = 1 and "TIDI_ID" = 6 order by 1 desc limit 1) is null
                	then 0
                	else (select "DISP_ID" from "DISPOSITIVOS" where "PUVE_ID" = p."PUVE_ID" and "DISP_ESTADO" = 1 and "TIDI_ID" = 6 order by 1 desc limit 1)
                end as dispows
                ,${dispositivo} as dispo
                ,'${clave}' as clave
                ,u."USUA_ID" as usuario
                ,u."USUA_REVERSOS" as reversos
                ,u."ACTIVO" as activo
                ,ig."INPE_APLICAHORARIO" as aplicahorario
                ,ig."INPE_HORARIOINICIO" as abre
                ,ig."INPE_HORARIOFINAL" as cierra
                ,ig."INPE_CIERREDIAS" as diasnohabiles
                ,ig."INPE_RAZONSOCIAL" as comercio_nombre
                ,ig."INPE_TELEFONO" as comercio_telefono
                ,gisc."TEXTO" as comercio_ciudad
                ,(SELECT max("INPE_CODIGOVERIFICACION") from "INFORMACIONGENERAL" where "ENTI_ID"=p."PUVE_ID") as codigoverificacion
                ,ig."INPE_EMAIL" as comercio_email
                ,case when (c."COME_TOKENATHM" = '') then false else true end as withathm
                ,(select short_link from links.all_links where come=p."COME_ID" order by 1 desc limit 1) as link
            from "USUARIO" u 
            inner join "INFORMACIONGENERAL" i on i."INPE_ID" = u."INPE_ID"
            inner join "PUNTODEVENTA" p on p."PUVE_ID" = i."ENTI_ID" 
            inner join "DISPOSITIVOS" d on d."PUVE_ID" = p."PUVE_ID"
            inner join "COMERCIO" c on c."COME_ID" = p."COME_ID" 
            inner join "INFORMACIONGENERAL" ig on ig."ENTI_ID" = c."COME_ID" and ig."INPE_PRINCIPAL" = 1
            left join "GIS"."CIUDAD_DEPTO" gis on gis."CIUDEPTO_ID" = c."CIUDEPTO_ID" 
            left join "GIS"."CIUDAD" gisc on gisc."CIUDAD_ID" = gis."CIUDAD_ID" 
            where 
            u."USUA_CLAVE" = md5('${clave}')
            and d."DISP_ID"  = ${dispositivo};`;
};

export const verifyPass = (clave: string, dispositivo: any, aux: any) => {
  let where: any;
  if (aux == 1){
    where = `u."USUA_ID" = ${clave} and d."DISP_ID"  = ${dispositivo};` ;
  }else if (aux == 2) {
    where = `u."USUA_CLAVE" = md5('${clave}') and d."DISP_ID"  = ${dispositivo};` ;
  }
  return `select count(*) as existe
          from "USUARIO" u 
          inner join "INFORMACIONGENERAL" i on i."INPE_ID" = u."INPE_ID"
          inner join "PUNTODEVENTA" p on p."PUVE_ID" = i."ENTI_ID" 
          inner join "DISPOSITIVOS" d on d."PUVE_ID" = p."PUVE_ID"
          where ${where}`;
};

export const getMd5Pass = (dispositivo: any, usua: string,) => {
  return `select u."USUA_CLAVE" as passmd5
          from "USUARIO" u 
          inner join "INFORMACIONGENERAL" i on i."INPE_ID" = u."INPE_ID"
          inner join "PUNTODEVENTA" p on p."PUVE_ID" = i."ENTI_ID" 
          inner join "DISPOSITIVOS" d on d."PUVE_ID" = p."PUVE_ID"
          where 
          u."USUA_ID" = ${usua}
            and d."DISP_ID"  = ${dispositivo};`;
};

export const loginApp = (clave: string, dispositivo: any) => {
  return `select i."INPE_PRIMERNOMBRE" 
                ,i."INPE_PRIMERAPELLIDO"
                ,d."DISP_ID" 
                ,p."PUVE_CODIGO"
                ,p."PUVE_ID"
                ,p."COME_ID"
                ,d."DISP_ID" as dispo
                ,'${clave}' as clave
            from "USUARIO" u 
            inner join "INFORMACIONGENERAL" i on i."INPE_ID" = u."INPE_ID"
            inner join "PUNTODEVENTA" p on p."PUVE_ID" = i."ENTI_ID" 
            inner join "DISPOSITIVOS" d on d."PUVE_ID" = p."PUVE_ID"
            inner join "DISPOSITIVOS_CELULAR" dc on dc."DISPOSITIVO" = d."DISP_ID"
            where 
            u."USUA_CLAVE" = md5('${clave}')
            and dc."CELULAR"  = '${dispositivo}';`;
};

/*export const getInfo = (comercio: any) => {
  return `select	c2."CUEN_SALDO"         as comeSaldo
                  ,i."INPE_PRIMERNOMBRE"  as comeNombre
                  ,i."INPE_TELEFONO"      as comeTelefono
          from "COMERCIO" c 
          inner join "CUENTA" c2 on c2."CUEN_ID" = c."CUEN_ID"
          inner join "INFORMACIONGENERAL" i on i."ENTI_ID" = c."COME_ID" and i."INPE_PRINCIPAL" = 1
          where c."COME_ID"  = ${comercio};`;
};*/
export const getInfo = (comercio: any, dispositivo: any, clave: string) => {
  return `select 			initcap(i."INPE_PRIMERNOMBRE") || ' ' || initcap(i."INPE_PRIMERAPELLIDO") || ' (' || u."USUA_CUENTA" || ')' as comeNombre
                     ,i."INPE_TELEFONO"      as comeTelefono
                     ,(select c2."CUEN_SOBREGIRO" + c2."CUEN_SALDO"  from "COMERCIO" c  inner join "CUENTA" c2 on c2."CUEN_ID" = c."CUEN_ID"  where c."COME_ID"  = ${comercio}) as comeSaldo
          from "USUARIO" u 
          inner join "INFORMACIONGENERAL" i on i."INPE_ID" = u."INPE_ID"
          inner join "PUNTODEVENTA" p on p."PUVE_ID" = i."ENTI_ID" 
          inner join "DISPOSITIVOS" d on d."PUVE_ID" = p."PUVE_ID"
          where 
          u."USUA_CLAVE" = md5('${clave}')
          and d."DISP_ID"  ='${dispositivo}';`
}

export const createComercio = (nombre: any, apellido: any, celular: any, email: any, clave: any) => {
  return `select app_creacomercio('${nombre}','${apellido}','${celular}','${email}','${clave}');`;
};

export const findCajero = (clave: string, dispositivo: any) => {
  return `select	p."PUVE_CODIGO"
                  ,(select "INPE_PRIMERNOMBRE" || ' ' || "INPE_PRIMERAPELLIDO"  from "INFORMACIONGENERAL" where "ENTI_ID"=p."PUVE_ID" and "INPE_PRINCIPAL"=1)  as "PDV"
                  ,d."DISP_ID" 
                  ,'${clave}' as "PASS"
          FROM "USUARIO" u
          inner join "INFORMACIONGENERAL" ig on ig."INPE_ID" = u."INPE_ID"
          inner join "PUNTODEVENTA" p on p."PUVE_ID" = ig."ENTI_ID" 
          inner join "DISPOSITIVOS" d on d."PUVE_ID" = p."PUVE_ID" 
          where ig."INPE_EMAIL" = '${dispositivo}'
                and u."USUA_CLAVE" = md5('${clave}')
                and d."TIDI_ID" = 4
                and d."DISP_ESTADO" = 1
                and p."COME_ID" = 2422
                and u."USUA_SUPERCAJERO" = 1
                and u."ACTIVO" in (1,2)
          group by 1,2,3,4
          ; `;
};

export const queryChangePass = (usuario: any, newPass: any) => {
  return `update "USUARIO" set "USUA_CLAVE"= md5('${newPass}'), "ACTIVO"=1 where "USUA_ID"= ${usuario}`;
};

export const queryPreRegister = (fname: any, lname: any, cell: any, email: any, nameComer: any, nameLegal: any, address: any, country: any) => {
  return `select registra_preregistro('${fname}','${lname}','${cell}','${email}','${nameComer}','${nameLegal}','${address}','${country}');`;
};

export const queryAllUserPdvs = (come: any) => {
  return `select 	(select "INPE_PRIMERNOMBRE" || ' ' || "INPE_PRIMERAPELLIDO"  from "INFORMACIONGENERAL" where "INPE_PRINCIPAL"='1' and "ENTI_ID"="INF"."ENTI_ID") as pdv
                  ,"INF"."ENTI_ID"		as puveid
                  ,"USU"."USUA_ID"	as cajeroid
                  ,"USU"."ACTIVO"		as estado
                  ,"INF"."INPE_PRIMERNOMBRE" || ' ' ||"INF"."INPE_PRIMERAPELLIDO"		as cajero
          from "public"."INFORMACIONGENERAL" as "INF" 
          inner join "public"."USUARIO" as "USU" on "INF"."INPE_ID"="USU"."INPE_ID" 
          where "INF"."INPE_PRINCIPAL"='2' 
                and "INF"."ENTI_ID" in (select "PUVE_ID" from "PUNTODEVENTA" where "COME_ID" = ${come})
                and "USU"."USUA_ID" != 8040
                and "USU"."ACTIVO"<>99 
          order by 1;`;
};

export const queryUpdateUserPdv = (usuario: any, estado: any) => {
  return `update "USUARIO" set "ACTIVO"='${estado}' where "USUA_ID"= ${usuario}`;
};

export const queryLoginVerification = (disp: any, puveid: any, ippub: any, ippriv: any) => {
  return `select codigoverificacion_activacionip('${disp}','${puveid}','${ippub}','${ippriv}');`;
};

export const queryInsertCodigo = (disp: any, puveid: any, ippub: any, ippriv: any, codigo: any) => {
  return `INSERT INTO portal.accesos_pc(terminal, puvecodigo, ippublica, ipprivada, estado, coigo) VALUES ('${disp}', '${puveid}', '${ippub}', '${ippriv}', 0, '${codigo}');`;
};


export const queryCodVerification = (disp: any, puveid: any, ippub: any, ippriv: any, code: any) => {
  return `
  SELECT	pc.estado AS "estado"
      FROM portal.accesos_pc as pc
      where pc.terminal = '${disp}' and pc.puvecodigo = '${puveid}' and pc.ippublica = '${ippub}' and pc.ipprivada = '${ippriv}' and coigo= '${code}';`;
};


export const queryUpdateCodVerification = (disp: any, puveid: any, ippub: any, ippriv: any, code: any) => {
  return `update portal.accesos_pc set estado = 1 where terminal = '${disp}' and puvecodigo = '${puveid}' and ippublica = '${ippub}' and ipprivada = '${ippriv}' and coigo= '${code}';`;
};


export const infoExternalLogin = (apiKey: string) => {
  return `select	d."DISP_ID" 
              ,u."USUA_CLAVE" 
          from "ACCESS_APIKEYS" aa
          inner join "USUARIO" u on u."USUA_ID" = aa."USUA_ID"
          inner join "INFORMACIONGENERAL" i on i."INPE_ID" = u."INPE_ID"  and u."ACTIVO" = 1
          inner join "DISPOSITIVOS" d on d."PUVE_ID" = i."ENTI_ID" and d."TIDI_ID" = 6 and d."DISP_ESTADO" = 1
          where aa."API_KEY" = '${apiKey}'
          limit 1;
  `
}


export const infoLoginSecurity = (password: string, dispositive: number, external: boolean) => {

  let where;
  if(external){
      where = ` u."USUA_CLAVE" = '${password}' and d."DISP_ID"  = ${dispositive};`;
  }else{
      where = ` u."USUA_CLAVE" = md5('${password}') and d."DISP_ID"  = ${dispositive};`;
  }

  return `select i."INPE_PRIMERNOMBRE" 
                ,i."INPE_PRIMERAPELLIDO"
                ,d."DISP_ID" 
                ,p."PUVE_CODIGO"
                ,p."PUVE_ID"
                ,p."COME_ID"
                ,case when (select "DISP_ID" from "DISPOSITIVOS" where "PUVE_ID" = p."PUVE_ID" and "DISP_ESTADO" = 1 and "TIDI_ID" = 6 order by 1 desc limit 1) is null
                    then 0
                    else (select "DISP_ID" from "DISPOSITIVOS" where "PUVE_ID" = p."PUVE_ID" and "DISP_ESTADO" = 1 and "TIDI_ID" = 6 order by 1 desc limit 1)
                end as dispows
                ,${dispositive} as dispo
                ,'${password}' as clave
                ,u."USUA_ID" as usuario
                ,u."USUA_REVERSOS" as reversos
                ,u."ACTIVO" as activo
                ,ig."INPE_APLICAHORARIO" as aplicahorario
                ,ig."INPE_HORARIOINICIO" as abre
                ,ig."INPE_HORARIOFINAL" as cierra
                ,ig."INPE_CIERREDIAS" as diasnohabiles
                ,ig."INPE_RAZONSOCIAL" as comercio_nombre
                ,ig."INPE_TELEFONO" as comercio_telefono
                ,gisc."TEXTO" as comercio_ciudad
                ,(SELECT max("INPE_CODIGOVERIFICACION") from "INFORMACIONGENERAL" where "ENTI_ID"=p."PUVE_ID") as codigoverificacion
                ,ig."INPE_EMAIL" as comercio_email
            from "USUARIO" u 
            inner join "INFORMACIONGENERAL" i on i."INPE_ID" = u."INPE_ID"
            inner join "PUNTODEVENTA" p on p."PUVE_ID" = i."ENTI_ID" 
            inner join "DISPOSITIVOS" d on d."PUVE_ID" = p."PUVE_ID"
            inner join "COMERCIO" c on c."COME_ID" = p."COME_ID" 
            inner join "INFORMACIONGENERAL" ig on ig."ENTI_ID" = c."COME_ID" and ig."INPE_PRINCIPAL" = 1
            left join "GIS"."CIUDAD_DEPTO" gis on gis."CIUDEPTO_ID" = c."CIUDEPTO_ID" 
            left join "GIS"."CIUDAD" gisc on gisc."CIUDAD_ID" = gis."CIUDAD_ID" 
            where ${where};`;
};
