export const getInfoTrx = (idTransaccion: any) => {
    return `select	t."TRAN_ID"			as idRec
                    ,t2."TRRE_NUMERO"	as numeroRec
                    ,t."TRAN_VALOR"		as valorRec
                    ,t."TRAN_IVU"		as ivuRec
                    ,p."PROD_ID" 	as prodidrec
                    ,p."PROD_DESCRIPCION" 	as prodRec
                    ,t."TRAN_FECHAREGISTRO"	as fechrec
                    --,i."INPE_PRIMERNOMBRE" || ' ' || i."INPE_PRIMERAPELLIDO" as nomCome
                    ,i."INPE_RAZONSOCIAL" as nomCome
		            ,i."INPE_TELEFONO"	as telCome
            from "TRANSACCION" t 
            inner join "PRODUCTO" p on p."PROD_ID" = t."PROD_ID"
            inner join "TRANSACCIONRECARGA" t2 on t2."TRAN_ID" = t."TRAN_ID"
            inner join "INFORMACIONGENERAL" i on i."ENTI_ID" = t."COME_ID" and i."INPE_PRINCIPAL" = 1
            where	t."TRAN_ID"::varchar like  '%${idTransaccion}';`;
};

export const existeTrx = (numero:any, valor:any, comercio:any, fecha:any) => {
    return `select	count(*)
            from "TRANSACCION" t 
            inner join "TRANSACCIONRECARGA" t2 on t2."TRAN_ID" = t."TRAN_ID" 
            where	t2."TRRE_NUMERO" = '${numero}'
                    and t."TRAN_VALOR" like '${valor}%'
                    and t."COME_ID" = ${comercio}
                    and t."TRAN_FECHA" = '${fecha}';`;
};

export const queryExecProcesar = (tipoTrx:string, trace:string, come:string, puvecodigo:string, terminal:string, timestamp:any, clave:string, idprod:string, valor:string) => {
    console.log(`SELECT * FROM procesar('${tipoTrx}', '${trace}', '${come}', '${puvecodigo}', '${terminal}', '${timestamp}', '${clave}', '${idprod}', '${valor}');`);
    return `SELECT * FROM procesar('${tipoTrx}', '${trace}', '${come}', '${puvecodigo}', '${terminal}', '${timestamp}', '${clave}', '${idprod}', '${valor}');`;
};

export const queryExecProcesarRecarga = (trx1:string, numero:string, code:string, transactionId:string, estado:string) => {
    return `SELECT * FROM procesarrecarga('${trx1}', '${numero}', '${code}', '${transactionId}', '${estado}');`;
};

export const queryGetProductByCode = (code: any) => {
    return `select "PROD_CODIGOORIGEN" as originCode from "PRODUCTO" where "PROD_CODIGO" ='${code}'; `;
};

export const queryUpdateTrxVersion = (trx: any, version: any) => {
    return `update "TRANSACCION" set "VERSION_PORTAL" = '${version}' where "TRAN_ID"::varchar like '%${trx}'`;
};

export const queryGetMargenMerchantFee = () => {
    return `select "PROD_VALOR" as margenmerchantfee from "PRODUCTO" where "PROD_CODIGO" = '${process.env.PRODUCTO_MERCHANFEE}'; `;
};

export const queryExecProcesarAux = (come: any, puveCod: any, disp: any, valor: any) => {
    return `	select * from procesar_auxiliar('04'::character varying
                            , '1'::character varying
                            , ${come}::character varying
                            , ${puveCod}::character varying
                            , ${disp}::character varying
                            , '0000000000'::character varying
                            , '000000'::character varying
                            , ${process.env.PRODUCTO_MERCHANFEE}::character varying
                            , ${valor}::decimal(10,2));`;
};