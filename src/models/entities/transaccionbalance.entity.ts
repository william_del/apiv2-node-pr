import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("TRANSACCION_BALANCE", { schema: "public" })
export class TransaccionBalance extends BaseEntity {
	@PrimaryGeneratedColumn({ type: "int", name: "TRBA_ID" })
	id: number;

    @Column("int", { name: "COME_ID"})
	comercio: number;

    @Column("float", { name: "VALOR"})
	valor: string;

    @Column("varchar", { name: "UNIQUE_ID"})
	idInterno: string;

    @Column("varchar", { name: "TRAN_ID_PASARELA" })
	trxPasarela: string;

    @Column("varchar", { name: "FECHA" })
	fecha: string;

    @Column("varchar", { name: "STATUS" })
	estado: number;
}