export interface IRecharge {
  //username: string;
  password?: string;
  numero: string;
  //process: string;
  valor: string;
  operador: string;
  proveedor?: string;
  tipo?: string;
  idprod?: string;
  merchanfee?: number;

  //CSQ (Integracion Bonos)
  name?: string;
  document?: string;
}