export interface IBalance {
  valor: string;
  pasarela: string;
  //idtrxwebhook?: string;
  estado?: string;
  celular?: string;

  subtotal?: string;
  tax?: string;
  total?: string;
  idtrxwebhook?: string;
  phonePay?: string;
}
