export interface IToken {
  INPE_PRIMERNOMBRE: string;
  INPE_PRIMERAPELLIDO: string;
  DISP_ID: number;
  PUVE_CODIGO: number;
}

export interface CreateTokenUserInterface {
  INPE_PRIMERNOMBRE: string;
  INPE_PRIMERAPELLIDO: string;
  DISP_ID: number;
  PUVE_CODIGO: number;
  PUVE_ID: number;
  COME_ID: number;
  dispows: number;
  dispo: number;
  clave: string;
  usuario: number;
  reversos: number;
  activo: boolean;
  aplicahorario: boolean;
  abre: string;
  cierra: string;
  diasnohabiles: boolean;
  comercio_nombre: string;
  comercio_telefono: string;
  comercio_ciudad: string;
  codigoverificacion: number;
  comercio_email: string;
}
