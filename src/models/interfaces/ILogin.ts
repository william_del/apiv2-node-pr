export interface ILogin {
  dispositivo: string;
  clave: string;
  app?: boolean;
  portal?: boolean;
}
