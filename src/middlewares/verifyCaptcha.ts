import axios from 'axios';

export async function verifyRecaptcha(token: string, remoteIp: string) {
    //const secretKey = '6LdyUdUpAAAAAO-FlS2u-VRDOzdyXczpH56Ip3mK';
    const secretKey = process.env.RECAPTCHA_SECRET_KEY;
    
    const url = `https://www.google.com/recaptcha/api/siteverify`;

    try {
        const response = await axios.post(url, {}, {
            params: {
                secret: secretKey,
                response: token,
                //remoteip: remoteIp
            }
        });
        //console.log("🚀 ~ verifyRecaptcha ~ response:", response)
        return response.data.success && response.data.score > 0.5; // Adjust score threshold as needed
    } catch (error) {
        console.error('Recaptcha verification failed:', error);
        return false;
    }
}