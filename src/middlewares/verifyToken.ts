import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";
import { decryptString } from "../util/auth/authEncode";
import moment from 'moment';

import pool from "../config/databasev2";
import * as USUARIO from "../models/querys/USUARIO";
import { CreateTokenUserInterface } from "../models/interfaces/IToken";
import blacklistService from "../util/auth/blackList";

export const verifyToken = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const authHeader = req.headers.authorization;
  const clientHash = req.headers.hash as string;
  const clientType = req.headers.client as string || 'internal';
  console.log("🚀 ~ clientType:", clientType)
  //const clientIp = req.ip;
  let clientIp = req.headers['x-forwarded-for'] || 'none';
  clientIp = clientIp.toString();
  
  let token;

  /*if (blacklistService.isBlacklisted(clientIp)) {
    return res.status(401).send("Access Denied: IP blocked due to suspicious activity");
  }

  if (!clientHash) {
    blacklistService.recordFailedAttempt(clientIp);
    return res.status(401).send("Access Denied: No hash provided");
  }

  if (!clientType) {
    blacklistService.recordFailedAttempt(clientIp);
    return res.status(401).send("Access Denied: No client type provided");
  }*/

  if(clientType == 'internal'){
    if ( true ) {
    /*if ( clientHash.match(/^[a-f0-9]{32}-[a-f0-9]{32}$/) ) {
      token = clientHash;
      const decrypted = await decryptString(token);
      console.log("🚀 ~ decrypted:", decrypted)
      const decryptedMoment = moment.unix(parseInt(decrypted));
      console.log("🚀 ~ decryptedMoment:", decryptedMoment)
      const currentMoment = moment(); 
      console.log("🚀 ~ currentMoment:", currentMoment)
      const timeDifference = currentMoment.diff(decryptedMoment, 'seconds');
      console.log("🚀 ~ timeDifference:", timeDifference)
      if (Math.abs(timeDifference) > 50) {
        blacklistService.recordFailedAttempt(clientIp);
        return res.status(401).send("Access Denied: time not valid");
      }*/

      if(authHeader){
        const parts = authHeader.split(" ");
        token = parts[1]; 
        try {
          const payload = jwt.verify(
            token,
            process.env.SECRET_TOKEN || "SECRET_TOKEN"
          );
          req.body.payload = payload;
        } catch (err: any) {
          console.log("🚀 ~ err:", err)
          if (err.name === "TokenExpiredError") {
            return res.status(401).send("Token Expired");
          } else return res.status(401).send("Access Denied.");
        }
      }

    }/*else {
      blacklistService.recordFailedAttempt(clientIp);
      return res.status(401).send("Access Denied: Invalid token format");
    }*/
  }

  if(clientType == 'external'){
    if ( clientHash.match(/^[a-f0-9]{32}-[a-f0-9]{32}$/) ) {
      token = clientHash;
      let inApiKey = req.headers.key;

      if (!inApiKey){
        blacklistService.recordFailedAttempt(clientIp);
        return res.status(401).send("Access Denied: No key provided");
      } 

      const decrypted = await decryptString(token);
      console.log("🚀 ~ decrypted:", decrypted)
      const decryptedMoment = moment.unix(parseInt(decrypted));
      console.log("🚀 ~ decryptedMoment:", decryptedMoment)
      const currentMoment = moment(); 
      console.log("🚀 ~ currentMoment:", currentMoment)
      const timeDifference = currentMoment.diff(decryptedMoment, 'seconds');
      console.log("🚀 ~ timeDifference:", timeDifference)
      if (Math.abs(timeDifference) > 10) {
        blacklistService.recordFailedAttempt(clientIp);
        return res.status(401).send("Access Denied: time not valid");
      }

      const resultInfoExternalLogin = await pool.query(USUARIO.infoExternalLogin(inApiKey.toString()));
      if (resultInfoExternalLogin.rows.length === 0) { 
        blacklistService.recordFailedAttempt(clientIp);
        return res.status(401).send("Access Denied: Invalid key"); 
      }

      const password =  resultInfoExternalLogin.rows[0].USUA_CLAVE;
      const dispositive =  resultInfoExternalLogin.rows[0].DISP_ID;
      const external = true;
      const resultInfoLogin = await pool.query(USUARIO.infoLoginSecurity(password, dispositive, external));
      const generateInternalToken = await generateToken(resultInfoLogin.rows[0]);

      const payload = jwt.verify(
        generateInternalToken,
        process.env.SECRET_TOKEN || "SECRET_TOKEN"
      );
      req.body.payload = payload;

    }else {
      blacklistService.recordFailedAttempt(clientIp);
      return res.status(401).send("Access Denied: Invalid token format");
    }
  }


  next();
};


async function generateToken(token: CreateTokenUserInterface) {
  return jwt.sign(token, process.env.SECRET_TOKEN || "SECRET_TOKEN", {
    expiresIn: '24h',
  });
}