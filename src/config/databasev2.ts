import env from "../config/enviroments";

const { Pool } = require("pg");
const pool = new Pool({
  user: env.DATABASE.username,
  host: env.DATABASE.host,
  database: env.DATABASE.name,
  password: env.DATABASE.password,
  port: env.DATABASE.port,
});

export default pool;
