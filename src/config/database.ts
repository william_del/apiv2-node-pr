import { createConnection } from "typeorm";
import env from "../config/enviroments";

class Database {
  constructor() {}
  async connectionDB() {
    const connection = await createConnection({
      name: "default",
      type: "postgres",
      host: env.DATABASE.host,
      port: env.DATABASE.port,
      username: env.DATABASE.username,
      password: env.DATABASE.password,
      database: env.DATABASE.name,
      synchronize: true,
      logging: false,
      entities: process.env.LOCAL
        ? ["src/models/entities/*.entity{.ts,.js}"]
        : ["dist/models/entities/*.entity{.ts,.js}"],
    })
      .then(() => {
        console.log("Connection to DB Create Sucessfully");
      })
      .catch((err: any) => {
        console.log(err);
        console.log({
          message:
            "Invalid credentials, check PORT, HOST, USERNAME, PASSWORD or DATABASE_NAME to access",
        });
      });
  }
}

export default new Database();
