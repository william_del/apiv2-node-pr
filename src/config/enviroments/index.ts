import dotenv from "dotenv";
dotenv.config();
import production from "./production";
import develop from "./develop";
const { NODE_ENV } = process.env;

let currentEnviroment = develop;

if (NODE_ENV === "production") {
  currentEnviroment = production;
}

export default currentEnviroment;
