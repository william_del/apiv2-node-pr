export default {
  PORT: process.env.PORT,
  DATABASE: {
    host: process.env.DATABASE_HOST_DEV,
    port: parseInt(process.env.DATABASE_PORT_DEV ?? "5432"),
    username: process.env.DATABASE_USERNAME_DEV,
    password: process.env.DATABASE_PASSWORD_DEV,
    name: process.env.DATABASE_NAME_DEV,
  },
};
