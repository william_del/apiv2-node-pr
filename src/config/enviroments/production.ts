export default {
  PORT: process.env.PORT,
  DATABASE: {
    host: process.env.DATABASE_HOST_PROD,
    port: parseInt(process.env.DATABASE_PORT_PROD ?? "5432"),
    username: process.env.DATABASE_USERNAME_PROD,
    password: process.env.DATABASE_PASSWORD_PROD,
    name: process.env.DATABASE_NAME_PROD,
  },
};
