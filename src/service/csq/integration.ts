import axios from "axios";
import moment from "moment";
import CryptoJS from 'crypto-js';
import { IRecharge } from "../../models/interfaces/IRecharge";
import { getProductByCode } from "../../util/functionsDb";

let csq_u = process.env.CSQ_U || '';
let csq_p = process.env.CSQ_PASS || '';

class CsqService{

    
    //#region INTEGRACION RECARGAS
    async rechagueCsq(infoRequest: IRecharge, transaccionIn: any){
        try {
            let url = process.env.CSQ_URL || '';
            let path_sale_prepaid = process.env.CSQ_PATH_SALE_PREPAID || '';
            //let csq_u = process.env.CSQ_U || '';
            let csq_terminal = process.env.CSQ_TERMINAL || '';

            const st = moment().unix().toString();
            let pass_sha = CryptoJS.SHA256(csq_p).toString();
            let salt_sha = CryptoJS.SHA256(st).toString();
            let csq_sh = CryptoJS.SHA256(pass_sha+salt_sha).toString();
            let csq_st = st;
            let csq_localref = moment().format(('HHMMSSss'));
            let csq_localdate = moment().format(('YYYYMMDD')).toString();
            let csq_localtimne = moment().format(('HHMMss'));

            let trxCsq: any;

            console.log(infoRequest.operador);
            const prodOriginCode = await getProductByCode(infoRequest.operador);
            console.log("🚀 ~ file: integration.ts:33 ~ CsqService ~ rechagueCsq ~ prodOriginCode:", prodOriginCode)

            console.log(url+path_sale_prepaid+csq_terminal
                +'/'+transaccionIn
                +'/'+csq_localdate
                +'/'+csq_localtimne
                +'/'+prodOriginCode
                +'/'+(Number(infoRequest.valor)*100)
                +'/'+infoRequest.numero);

            await axios.get(url+path_sale_prepaid+csq_terminal
                +'/'+csq_localref
                +'/'+csq_localdate
                +'/'+csq_localtimne
                +'/'+prodOriginCode
                +'/'+(Number(infoRequest.valor)*100)
                +'/'+infoRequest.numero, {
                headers: {
                    'U': csq_u,
                    'SH': csq_sh,
                    'ST': csq_st
                }
            }).then((response) => {
                trxCsq = response.data;
            }).catch((error) => {
                trxCsq =  error.response.data;
            });

            /*let aux = {
                rc: 0,
                items: [
                  {
                    finalstatus: 0,
                    resultcode: '10',
                    resultmessage: 'Operación efectuada correctamente',
                    supplierreference: '0000550388',
                    suppliertoken: ''
                  }
                ]
            }*/
            return trxCsq;
        } catch (error) {
            return 0;
        }
            
    }
    //#endregion

    //#region INTEGRACION SUPERMARKET
    async validateDocCsq(doc: any){
        try {
            let url = process.env.CSQ_URL || '';
            let path_validatedoc = process.env.CSQ_VALIDATEDOC_SM || '';
            //let csq_u = process.env.CSQ_U_SM || '';

            const st = moment().unix().toString();
            let pass_sha = CryptoJS.SHA256(csq_p).toString();
            let salt_sha = CryptoJS.SHA256(st).toString();
            let csq_sh = CryptoJS.SHA256(pass_sha+salt_sha).toString();
            let csq_st = st;
            let csq_localref = moment().format(('HHMMSSss'));
            let csq_localdate = moment().format(('YYYYMMDD')).toString();
            let csq_localtimne = moment().format(('HHMMss'));

            console.log(url + path_validatedoc + doc);

            const response = await axios.get(url + path_validatedoc + doc, {
                headers: {
                    'U': csq_u,
                    'SH': csq_sh,
                    'ST': csq_st
                }
            });        
            return response.data;

        } catch (error: any) {
            console.log("🚀 ~ file: integration.ts ~ CsqService ~ validateDocCsq ~ error:", error)
            return 0;
        }
    }

    async registerSMCsq(body: any){
        try {
            let url = process.env.CSQ_URL || '';
            let path_getPurchase = process.env.CSQ_GETPURCHASE_SM || '';
            let path_register = process.env.CSQ_REGISTER_SM || '';
            let terminal = process.env.CSQ_TERMINAL_SM || '';
            //let csq_u = process.env.CSQ_U_SM || '';
            let productSm = process.env.CSQ_ID_SM || '';
            //let csq_terminal = process.env.CSQ_TERMINAL_SM || '';

            const st = moment().unix().toString();
            let pass_sha = CryptoJS.SHA256(csq_p).toString();
            let salt_sha = CryptoJS.SHA256(st).toString();
            let csq_sh = CryptoJS.SHA256(pass_sha+salt_sha).toString();
            let csq_st = st;
            let csq_localref = moment().format(('HHMMSSss'));
            let csq_localdate = moment().format(('YYYYMMDD')).toString();
            let csq_localtimne = moment().format(('HHMMss'));

            console.log(url + path_getPurchase + terminal + '/' + body.operador + '/' + body.valor*100);

            const responsePurchase: any = await axios.get(url + path_getPurchase + terminal + '/' + body.operador + '/' + body.valor*100, {
                headers: {
                    'U': csq_u,
                    'SH': csq_sh,
                    'ST': csq_st
                }
            });
            console.log("🚀 ~ file: integration.ts ~ CsqService ~ registerSMCsq ~ responsePurchase:", responsePurchase.data)

            const infoPurchase = {
                montoEnviar: responsePurchase.data.amountToSendX100,
                montoDestino: responsePurchase.data.destinationAmountX100,
                tarifaServicio: responsePurchase.data.serviceFeeX100,
                montoTotal: responsePurchase.data.totalAmountX100,
                codeRegister: 0,
            };

            const nameBase64 = Buffer.from(body.name, 'utf-8').toString('base64');

            const responseRegister: any = await axios.get(url + path_register + terminal + '/' + body.operador + '/' + body.document +'/'+ nameBase64 +'/1'+ body.numero +'/'+ infoPurchase.montoEnviar +'/'+ infoPurchase.montoDestino +'/'+ infoPurchase.tarifaServicio +'/'+ infoPurchase.montoTotal , {
                headers: {
                    'U': csq_u,
                    'SH': csq_sh,
                    'ST': csq_st
                }
            });

            infoPurchase.codeRegister = responseRegister.data.items[0].idtoken;

        
            return infoPurchase;

        } catch (error: any) {
            console.log(error.response.data);
            return 0;
        }
    }

    async saleSMCsq(body: any){
        try { //{{terminalid}}/{{localreference}}/{{localdate}}/{{localtime}}/1935/10000/620
            let url = process.env.CSQ_URL || '';
            let path_sale = process.env.CSQ_SALE_SM || '';
            //let csq_u = process.env.CSQ_U_SM || '';
            let csq_terminal = process.env.CSQ_TERMINAL_SM || '';
            let productSm = process.env.CSQ_ID_SM || '';

            const st = moment().unix().toString();
            let pass_sha = CryptoJS.SHA256(csq_p).toString();
            let salt_sha = CryptoJS.SHA256(st).toString();
            let csq_sh = CryptoJS.SHA256(pass_sha+salt_sha).toString();
            let csq_st = st;
            let csq_localref = moment().format(('HHMMSSss'));
            let csq_localdate = moment().format(('YYYYMMDD')).toString();
            let csq_localtimne = moment().format(('HHMMss'));

            console.log(body.data.montoTotal);
            console.log(body.data.montoTotal*100);
            console.log();
            console.log(url + path_sale + csq_terminal  +'/'+ csq_localref +'/'+ csq_localdate +'/'+ csq_localtimne +'/'+ productSm +'/'+ body.data.montoTotal*100 +'/'+ body.data.codeRegister)

            const responseSale: any = await axios.get(url + path_sale + csq_terminal  +'/'+ csq_localref +'/'+ csq_localdate +'/'+ csq_localtimne +'/'+ productSm +'/'+ body.data.montoTotal*100 +'/'+ body.data.codeRegister , {
                headers: {
                    'U': csq_u,
                    'SH': csq_sh,
                    'ST': csq_st
                }
            });

            return responseSale;

        } catch (error: any) {
            console.log(error.response.data);
            return 0;
        }
    }
    //#endregion

}


const csqService = new CsqService();
export default csqService;
