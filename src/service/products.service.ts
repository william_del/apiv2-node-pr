import pool from "../config/databasev2";
import { HttpResponse } from "../util/HttpResponse";
import * as PVP from "../models/querys/PUNTODEVENTAPRODUCTO";
import * as TRX from "../models/querys/TRANSACCION";
import * as USU from "../models/querys/USUARIO";
import { token } from "morgan";
import { IRecharge } from "../models/interfaces/IRecharge";
import csqService from "./csq/integration";

const date = require("date-and-time");

class ProductsService {

  //#region GETs
  async getInfoPaises(token: any, tipoProducto: any) {
    const httpResponse = new HttpResponse();
    let response;
    try {
      const query = PVP.infoPaises(
        token.PUVE_ID,
        token.PUVE_CODIGO,
        tipoProducto
      );
      response = await pool.query(query);

      if (response.rows.length > 0) {
        httpResponse.findAll(response.rows);
        return httpResponse;
      }

      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async getInfoProductos(token: any, paisId: any, tipoProducto: any) {
    console.log("🚀 ~ ProductsService ~ getInfoProductos ~ getInfoProductos:")
    const httpResponse = new HttpResponse();
    let response;
    let tipoTrx = '04';

    tipoTrx = tipoProducto === '1' ? '04' : '05';

    console.log('token.PUVE_ID '+token.PUVE_ID);
    console.log('token.PUVE_CODIGO '+token.PUVE_CODIGO);
    console.log('tipoProducto '+tipoProducto);
    console.log('tipoTrx '+tipoTrx);

    try {
      const query = PVP.infoProductos(
        token.PUVE_ID,
        token.PUVE_CODIGO,
        paisId,
        tipoProducto,
        tipoTrx
      );
      response = await pool.query(query);

      if (response.rows.length > 0) {
        let conteo = 0;
        const productsArray = Object.values(
          response.rows.reduce((item: any, currItem: any) => {
            var PROD_DESCRIPCION_SPLIT =
              currItem.PROD_DESCRIPCION.split(" ")[0];

            let urlLogo = currItem.PROD_SPLIT == null ? '' : `${process.env.URL_PLATFORM}${PROD_DESCRIPCION_SPLIT}.png`;

            if (!item[PROD_DESCRIPCION_SPLIT]) {
              conteo++;
              item[PROD_DESCRIPCION_SPLIT] = {
                name: PROD_DESCRIPCION_SPLIT,
                id: conteo,
                logo: urlLogo,
                productos: [],
              };
            }

            item[PROD_DESCRIPCION_SPLIT].productos.push({
              id: currItem.PROD_ID,
              code: currItem.PROD_CODIGO || currItem.PROD_CODIGOORIGEN,
              tipo: currItem.TIPR_ID,
              nombre: currItem.PROD_DESCRIPCION,
              valor: currItem.PROD_VALOR
                ? parseFloat(currItem.PROD_VALOR).toFixed(2)
                : parseFloat("0.0").toFixed(2),
              valorMin: currItem.PROD_MINIMO
                ? parseFloat(currItem.PROD_MINIMO).toFixed(2)
                : parseFloat("0.0").toFixed(2),
              valorMax: currItem.PROD_MAXIMO
                ? parseFloat(currItem.PROD_MAXIMO).toFixed(2)
                : parseFloat("0.0").toFixed(2),
              country: "",
              fee: currItem.PROD_FEE
                ? currItem.PROD_FEE
                : parseFloat("0.0").toFixed(1),

              topfive: currItem.TOPFIVE,

              caracteres: "",
              codigo: currItem.PROD_CODIGO || currItem.PROD_CODIGOORIGEN,
              descripcion: currItem.PROD_DESCRIPCION.split(" ")[0],
              ganancia: 0,
              puntodeventaId: token.PUVE_CODIGO,
              sms: "",
              terminos: "",
              proveedorId: currItem.PROV_ID,
              //   paisId: currItem.PAIS_ID,
              //   paisNombre: currItem.PAIS_NOMBRE,
            });

            return item;
          }, {})
        );

        httpResponse.findAll(productsArray);
        return httpResponse;
      }

      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async getInfoProductosOldVersion(
    token: any,
    locationId: any,
    tipoProducto: any,
    paisId: any
  ) {
    console.log("🚀 ~ ProductsService ~ getInfoProductosOldVersion:")
    const httpResponse = new HttpResponse();
    let response;
    try {
      const query = PVP.infoProductosOldVersion(
        token.PUVE_ID,
        Number(locationId),
        tipoProducto,
        paisId
      );
      response = await pool.query(query);

      if (response.rows.length > 0) {
        let conteo = 0;
        const productsArray = Object.values(
          response.rows.reduce((item: any, currItem: any) => {
            var PROD_DESCRIPCION_SPLIT =
              currItem.PROD_DESCRIPCION.split(" ")[0];

            if (!item[PROD_DESCRIPCION_SPLIT]) {
              conteo++;
              item[PROD_DESCRIPCION_SPLIT] = {
                name: PROD_DESCRIPCION_SPLIT,
                id: conteo,
                logo: `${process.env.URL_PLATFORM}${PROD_DESCRIPCION_SPLIT}.png`,
                productos: [],
              };
            }

            item[PROD_DESCRIPCION_SPLIT].productos.push({
              id: currItem.PROD_ID,
              code: currItem.PROD_CODIGO,
              tipo: currItem.TIPR_ID,
              nombre: currItem.PROD_DESCRIPCION,
              valor: currItem.PROD_VALOR
                ? parseFloat(currItem.PROD_VALOR).toFixed(2)
                : parseFloat("0.0").toFixed(2),
              valorMin: currItem.PROD_MINIMO
                ? parseFloat(currItem.PROD_MINIMO).toFixed(2)
                : parseFloat("0.0").toFixed(2),
              valorMax: currItem.PROD_MAXIMO
                ? parseFloat(currItem.PROD_MAXIMO).toFixed(2)
                : parseFloat("0.0").toFixed(2),
              longitud: currItem.PROD_LENGTH,
              tipoSub: currItem.TISP_ID,
              providerId: currItem.PROV_ID,
              sku: null,
              skurel: null,
              country: "",
              needsPin: false,
              fee: currItem.PROD_FEE
                ? currItem.PROD_FEE
                : parseFloat("0.0").toFixed(1),
              
              paisId: currItem.PAIS_ID,
              paisNombre: currItem.PAIS_NOMBRE,
            });

            return item;
          }, {})
        );

        httpResponse.findAll(productsArray);
        return httpResponse;
      }

      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async getInfoTrx(idTrx: any) {

    console.log("getInfoTrx");
    const httpResponse = new HttpResponse();
    let response;

    try {
      const query = TRX.getInfoTrx(idTrx);
      response = await pool.query(query);
      //console.log("🚀 ~ file: products.service.ts:211 ~ ProductsService ~ getInfoTrx ~ response:", response.rows)

      if (response.rows.length > 0) {
        //console.log("🚀 ~ file: products.service.ts:213 ~ ProductsService ~ getInfoTrx ~ response.rows:", response.rows)
        httpResponse.findOne(response.rows);
        return httpResponse;
      }

      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async existeTrx(token: any, numero: any, valor: any) {
    const now = new Date();
    var fechaHoy = date.format(now, "YYYY-MM-DD");

    const query = TRX.existeTrx(numero, valor, token.COME_ID, fechaHoy);

    //console.log('query'+query);

    let cantidadTrx = await pool.query(query);

    return cantidadTrx.rows[0].count;
  }

  async getMovimientos(token: any, fechaIni: any, fechaFin: any) {
    const httpResponse = new HttpResponse();
    let response;

    try {
      const query = PVP.getMovimientos(token.PUVE_ID, fechaIni, fechaFin);
      response = await pool.query(query);

      if (response.rows.length > 0) {
        httpResponse.findAll(response.rows);
        return httpResponse;
      }
      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }
  
  async getTopFive(token: any) {
    const httpResponse = new HttpResponse();
    let response;

    try {
      const query = PVP.getTopFive(token.COME_ID);
      response = await pool.query(query);

      if (response.rows.length > 0) {
        
        httpResponse.findAll(response.rows);
        return httpResponse;
      }
      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async getCountry() {
    const httpResponse = new HttpResponse();
    let response;

    try {
      const query = PVP.q_getCountry();
      response = await pool.query(query);

      if (response.rows.length > 0) {
        httpResponse.findAll(response.rows);
        return httpResponse;
      }
      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async getInfoTrPinManual(getInfoTrPinManual:any, request:IRecharge) {
    const httpResponse = new HttpResponse();
    try {
      
      const dataResponse = {
        idrec: getInfoTrPinManual.idTransaccion,
        numerorec: request.numero,
        valorrec: request.valor,
        serial: getInfoTrPinManual.serial,
        pin: getInfoTrPinManual.pin,
        fecexp: getInfoTrPinManual.fechaExpiracion,
        ivurec: "0.00",
        prodrec: "",
        fechrec: "",
        nomcome: "",
        telcome: ""
      };

      httpResponse.findOne(dataResponse);
      return httpResponse;
    } catch (error) {
      httpResponse.error(error);
      return httpResponse;
    }
  }
  //#endregion

  async verifyPass(token: any, clave: any) {
    const httpResponse = new HttpResponse();
    let response: any;
    try {
      let aux: any;
      if (clave.length === 4){
        aux = 1;
      }else{
        aux = 2;
      }
      const query = USU.verifyPass(clave, token.DISP_ID, aux);
      response = await pool.query(query);

      if (response.rows[0].existe == 1) {
        httpResponse.success("Clave correcta",{});
      }else{
        httpResponse.error("Clave incorrecta");
      }
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async saveMerchantfee(token: any, valor: any) {
    const httpResponse = new HttpResponse();
    try {
      //Consulta porcentaje producto merchantFee
      const queryMargen = TRX.queryGetMargenMerchantFee();
      let responseQueryMargen = await pool.query(queryMargen);
      const margenmerchantfee = responseQueryMargen.rows[0].margenmerchantfee;
      //valor = valor*25/100;

      const query = TRX.queryExecProcesarAux(token.COME_ID, token.PUVE_CODIGO, token.DISP_ID, margenmerchantfee);
      const response = await pool.query(query);
      console.log("🚀 ~ ProductsService ~ saveMerchantfee ~ response:", response.rows)
      if (response.rows[0].idtransaccion != null) {
        const query2 = TRX.queryExecProcesarRecarga(response.rows[0].idtransaccion, '1000000001', '00', valor, '1');
        const response2 = await pool.query(query2);
        console.log("🚀 ~ ProductsService ~ saveMerchantfee ~ response2:", response2.rows)
      }
      httpResponse.success("Ok",{});
      return httpResponse;
    } catch (error) {
      console.log("🚀 ~ ProductsService ~ saveMerchantfee ~ error:", error)
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async generateTrx(token: any, infoRequest: IRecharge) {    
    console.log("🚀 ~ ProductsService ~ generateTrx ~ infoRequest.tipo:", infoRequest.tipo)
    try {
      let tipoTrx;

      if (infoRequest.tipo == "1") {
          tipoTrx = '04';
      } else if (infoRequest.tipo == "3") {
          tipoTrx = '05';
      } else if (infoRequest.tipo == "21") {
          tipoTrx = '06';
      } else {
          tipoTrx = '04';
      }

      console.log("🚀 ~ ProductsService ~ generateTrx ~ tipoTrx:", tipoTrx)

      const trace = '00';
      const timestamp = Date.now();
      
      const query = TRX.queryExecProcesar(tipoTrx, trace, token.COME_ID, token.PUVE_CODIGO, token.dispo, timestamp, token.clave, infoRequest.operador, infoRequest.valor);
      let resultQuery = await pool.query(query);
      //console.log("🚀 ~ ProductsService ~ generateTrx ~ resultQuery:", resultQuery)
      return resultQuery.rows[0].idtransaccion;
    } catch (error) {
      console.log("🚀 ~ file: products.service.ts:329 ~ ProductsService ~ generateTrx ~ error", error)
      return 0;
    }
  }

  async updateTrx(token: any, infoRequest: IRecharge, trx1: any, trx2: any) {
    console.log("🚀 ~ ProductsService ~ updateTrx ~ trx2:", trx2)
    try {
      
      const estado = trx2.code == "00" ? "1" : "2";
      const query = TRX.queryExecProcesarRecarga(trx1, infoRequest.numero, trx2.code, trx2.transactionId, estado);
      await pool.query(query);
      //console.log("🚀 ~ file: products.service.ts:338 ~ ProductsService ~ generateTrx ~ resultQuery:", resultQuery)

      //return resultQuery.rows[0].responsecode;
    } catch (error) {
      console.log("🚀 ~ file: products.service.ts:329 ~ ProductsService ~ generateTrx ~ error", error)
      //return 0;
    }
  }

  async updateTrxVersion(trx: any, version: any) {
    try {
      const query = TRX.queryUpdateTrxVersion(trx, version);
      await pool.query(query);
    } catch (error) {
      console.log("🚀 ~ ProductsService ~ updateTrxVersion ~ error:", error)
    }
  }


  //#region CSQ
  async processCSQ(token: any, infoRequest: IRecharge, version: any){
    const httpResponse = new HttpResponse();
    try {
      
      const validateDocCsq = await csqService.validateDocCsq(infoRequest.document);
      if(validateDocCsq.resultcode == -1){
        httpResponse.personalized( false, validateDocCsq.resultmessage, {} );
      }

      const registerCsq = await csqService.registerSMCsq(infoRequest);
      console.log("🚀 ~ ProductsService ~ processCSQ ~ registerCsq:", registerCsq)


      return httpResponse;
    } catch (error) {
      
    }
  }

  

  async validateDoc(doc: any) {
    const httpResponse = new HttpResponse();
    try {
      
      const responseCsq = await csqService.validateDocCsq(doc);
      if(responseCsq == 0){
        httpResponse.notSuccess("Carrier error, try again",{});
      }else{
        httpResponse.success(responseCsq.resultmessage,responseCsq);
      }

      //httpResponse.findOne({});
      return httpResponse;
    } catch (error) {
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async validatePhone(num: any) {
    const httpResponse = new HttpResponse();
    try {

      const regexRD = /^(809|829|849)(\d{7})$/;
      const numeroLimpio = num.replace(/\s/g, '').replace(/-/g, '');
      const numeroValido = regexRD.test(numeroLimpio);
      console.log("🚀 ~ file: products.service.ts:428 ~ ProductsService ~ registerSM ~ numeroValido:", numeroValido)

      if(!numeroValido){
        httpResponse.error("Invalid cell phone number");
        return httpResponse;
      }
      
      httpResponse.success("Valid cell phone number",{});
      return httpResponse;
    } catch (error) {
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async registerSM(body: any) {
    const httpResponse = new HttpResponse();
    try {

      const responseCsq = await csqService.registerSMCsq(body);

      httpResponse.findAll(responseCsq);
      return httpResponse;
    } catch (error) {
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async saleSM(token: any, infoRequest: any){
    const httpResponse = new HttpResponse();
    try {
      interface IdTransaccionCsq {
        code: string;
        transactionId: string;
      }
      
      let idTransaccionCsq: IdTransaccionCsq = {
        code: "",
        transactionId: ""
      };

      let idTransaccionProcesar = await this.generateTrx(token, infoRequest);
      const responseCsq = await csqService.saleSMCsq(infoRequest);
      console.log("🚀 ~ file: products.service.ts:440 ~ ProductsService ~ saleSM ~ responseCsq:", responseCsq.data)

      if(responseCsq.data.rc == "0"){
        idTransaccionCsq = {
          code: "00",
          transactionId: responseCsq.data.items[0].supplierreference
        };
      }else{
        idTransaccionCsq = {
          code: "99",
          transactionId: responseCsq.data.items[0]?.resultcode || 'Error controlado'
        };
      }
      
      await this.updateTrx(token, infoRequest, idTransaccionProcesar, idTransaccionCsq);

      if(idTransaccionCsq.code == "99"){
        httpResponse.error("Carrier error");
      }else{
        const idTransaccion =  idTransaccionProcesar;
        return await this.getInfoTrx(idTransaccion);
      }

      //httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log("🚀 ~ file: products.service.ts:472 ~ ProductsService ~ saleSM ~ error:", error)
      httpResponse.error(error);
      return httpResponse;
    }
  }
  //#endregion

}



const productsService = new ProductsService();
export default productsService;
