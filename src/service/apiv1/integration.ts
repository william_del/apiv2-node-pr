import axios from "axios";
import { IRecharge } from "../../models/interfaces/IRecharge";
import { getPasswordMd5 } from "../../util/functionsDb";
import productsService from "../products.service";
import NodeCache from "node-cache";
import moment from "moment";

import { Client, LocalAuth } from 'whatsapp-web.js';
import { clients } from "../userService.service";

const transaccionesProcesadas = new NodeCache({ stdTTL: 3, checkperiod: 4 });


class Apiv1Service {

    async rechargeOldApi(token: any, infoRequest: IRecharge, transaccion: any) {
        //console.log("🚀 ~ file: integration.ts:8 ~ Apiv1Service ~ rechargeOldApi ~ recharge:")
        let tokenApiv1: any;
        let idTrx: any;
        let urlApiv1 = process.env.APIV1_URL || "https://apitest.pagatodopr.com";
        let pathLogin = urlApiv1 + (process.env.APIV1_PATHLOGIN || "/auth/login");

        //**  SI COMO QUERY PARAMS VIENE UN VALOR EN TRANSACCION SE REALIZA RECARGA DE EVERTEC **/

        let pathEvertec = transaccion
            ? `/api/recharge/evertec?transaccion=${transaccion}`
            : "/api/recharge/";
        let pathRecarga = urlApiv1 + pathEvertec;

        let dataLogin = {
            username: token.dispo,
            password: token.clave,
            portal: true,
        };
        console.log("🚀 ~ file: integration.ts:26 ~ Apiv1Service ~ rechargeOldApi ~ dataLogin:", JSON.stringify(dataLogin));

        try {
            await axios.post(pathLogin, dataLogin).then((response) => {
                tokenApiv1 = response.data.token;
            });
        } catch (error: any) {
            console.log("🚀 ~ file: integration.ts:36 ~ Apiv1Service ~ rechargeOldApi ~ error:", error.message)
        }


        let passMd5: any;
        if (infoRequest.password?.length === 4) {
            passMd5 = await getPasswordMd5(1, token.dispo, infoRequest.password);
        } else {
            passMd5 = infoRequest.password;
        }

        //console.log("🚀 ~ file: integration.ts:39 ~ Apiv1Service ~ rechargeOldApi ~ passMd5:", passMd5)

        let dataTrx = {
            username: token.dispows == 0 ? token.dispo : token.dispows,
            password: passMd5,
            numero: infoRequest.numero,
            process: "400003",
            valor: infoRequest.valor,
            operador: { codigo: infoRequest.operador },
        };
        console.log("comercio ", token.COME_ID, "🚀 ~ file: integration.ts:49 ~ Apiv1Service ~ rechargeOldApi ~ dataTrx:", JSON.stringify(dataTrx));

        var options: Object = {
            method: "POST",
            url: pathRecarga,
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + tokenApiv1,
            },
            data: dataTrx,
        };
        //console.log("🚀 ~ file: integration.ts:62 ~ Apiv1Service ~ rechargeOldApi ~ options:", options)

        let response: any;
        try {
            response = await axios.request(options);
            console.log("comercio ", token.COME_ID, "🚀 ~ file: integration.ts ~ Apiv1Service ~ rechargeOldApi ~ response:", response.data);
            idTrx = response.data.idTransaccion;
        } catch (e: any) {
            console.log("comercio ", token.COME_ID, "🚀 ~ file: integration.ts ~ Apiv1Service ~ rechargeOldApi ~ response:", e.response.data.message);
            idTrx = e.response.data.message;
        }
        //console.log("comercio ", token.COME_ID ,"🚀 ~ file: integration.ts:65 ~ Apiv1Service ~ rechargeOldApi ~ response:", response);
        //console.log("comercio ", token.COME_ID ,"🚀 ~ file: integration.ts:65 ~ Apiv1Service ~ rechargeOldApi ~ response:", response.data);
        /*try {
            await axios.request(options).then(function (response) {
                idTrx = response.data.idTransaccion;
            });
        } catch (e) {
            console.log("error: "+e);
        }*/

        console.log("🚀 ~ file: integration.ts:57 ~ Apiv1Service ~ resp ~ idTrx:", idTrx);
        return idTrx;
    }

    async pinOldApi(token: any, infoRequest: IRecharge, transaccion: any) {
        let tokenApiv1: any;
        let response: any;
        let idTrx: any;
        let urlApiv1 = process.env.APIV1_URL || "https://apitest.pagatodopr.com";
        let pathLogin = urlApiv1 + (process.env.APIV1_PATHLOGIN || "/auth/login");
        console.log("🚀 ~ file: integration.ts:66 ~ Apiv1Service ~ pinOldApi ~ pathLogin:", pathLogin)

        //**  SI COMO QUERY PARAMS VIENE UN VALOR EN TRANSACCION SE REALIZA RECARGA DE EVERTEC **/

        let pathEvertec = transaccion
            ? `/api/pin/evertec?transaccion=${transaccion}`
            : "/api/pin/";
        let pathRecarga = urlApiv1 + pathEvertec;

        let dataLogin = {
            username: token.dispo,
            password: token.clave,
            portal: true,
        };

        console.log("🚀 ~ file: integration.ts:26 ~ Apiv1Service ~ rechargeOldApi ~ dataLogin:", JSON.stringify(dataLogin));

        await axios.post(pathLogin, dataLogin).then((response) => {
            tokenApiv1 = response.data.token;
        });

        let dataTrx = {
            username: token.dispows == 0 ? token.dispo : token.dispows,
            password: token.clave,
            celular: infoRequest.numero,
            valor: infoRequest.valor,
            prodCodigo: infoRequest.operador,
        };
        console.log("comercio ", token.COME_ID, "🚀 ~ file: integration.ts:49 ~ Apiv1Service ~ rechargeOldApi ~ dataTrx:", JSON.stringify(dataTrx));

        var options: Object = {
            method: "POST",
            url: pathRecarga,
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + tokenApiv1,
            },
            data: dataTrx,
        };

        console.log("🚀 ~ file: integration.ts:102 ~ Apiv1Service ~ pinOldApi ~ dataTrx:", JSON.stringify(dataTrx));

        try {
            response = await axios.request(options);
            console.log("comercio ", token.COME_ID, "🚀 ~ file: integration.ts ~ Apiv1Service ~ rechargeOldApi ~ response:", response.data);
            idTrx = response.data.idTransaccion;
        } catch (e: any) {
            console.log("comercio ", token.COME_ID, "🚀 ~ file: integration.ts ~ Apiv1Service ~ rechargeOldApi ~ response:", e.response.data.message);
            idTrx = e.response.data.message;
        }

        console.log("🚀 ~ file: integration.ts:110 ~ Apiv1Service ~ resp ~ response:", response.data);
        return response.data;
    }

}

class ProcessTrx {
    async transactionProcess(token: any, infoRequest: IRecharge, version?: any) {
        try {
            let idTransaccion: any;

            const identificadorTransaccion = `${token.COME_ID}_${infoRequest.numero}_${infoRequest.valor}_${infoRequest.operador}`;

            if (transaccionesProcesadas.get(identificadorTransaccion)) {
                console.log(`La transacción ${identificadorTransaccion} ya ha sido procesada. Ignorando la solicitud.`);
                return { status: false, message: "Procesando transacción" };
            }

            console.log(`Procesando la transacción ${identificadorTransaccion}...`);

            transaccionesProcesadas.set(identificadorTransaccion, true);

            if (infoRequest.tipo == "1" || infoRequest.tipo == "15") {
                console.log("🚀 ~ file: products.controller.ts:146 ~ ProductsController ~ recharge ~ true:")
                await apiv1Service.rechargeOldApi(token, infoRequest, undefined).then((response) => {
                    idTransaccion = response;
                });

                if (!isNaN(idTransaccion) && typeof +idTransaccion === 'number') {

                    if (version) {
                        await productsService.updateTrxVersion(idTransaccion, version);
                        let valorMerchanFee = (infoRequest.merchanfee || 0)
                        if (valorMerchanFee > 0) {
                            await productsService.saveMerchantfee(token, valorMerchanFee);
                        }
                    }

                    const { status, message, data } = await productsService.getInfoTrx(idTransaccion);
                    console.log("🚀 ~ ProcessTrx ~ transactionProcess ~ data:", data)

                    const fechaRec = moment(data[0].fechrec);
                    const fechaFormateada = fechaRec.format('YYYY-MM-DD hh:mm:ss');


                    //const clients = {};
                    const comeId = token.COME_ID;
                    const sessionIds = [`whatsapp-session-${comeId}`];
                    console.log("🚀 ~ UserService ~ sendWpMsg ~ sessionIds:", sessionIds);

                    // Intentamos restaurar la sesión para el cliente específico
                    let client = clients[token.COME_ID];
                    // Si no hay cliente en memoria, lo restauramos
                    /*if (!client) {
                        console.log(`🔄 Cliente ${comeId} no está en memoria, intentando restaurar...`);

                        client = new Client({
                            authStrategy: new LocalAuth({ clientId: `whatsapp-session-${comeId}` })
                        });

                        client.on('ready', () => {
                            console.log(`✅ Cliente ${comeId} listo después de restauración`);
                            clients[comeId] = client;
                            clients[comeId].ready = true;
                        });

                        client.on('disconnected', () => {
                            console.log(`❌ Cliente ${comeId} desconectado`);
                            delete clients[comeId]; // Remueve de memoria
                        });

                        // Inicializamos el cliente y esperamos a que esté listo
                        await client.initialize();
                    }*/

                    const valorRec = Math.round(data[0].valorrec * 100);
                    const prodidrec = data[0].prodidrec.toString();
                    const paddedProdidrec = prodidrec.padStart(4, '0');
                    console.log("🚀 ~ ProcessTrx ~ transactionProcess ~ data[0].numerorec:", data[0].numerorec)
                    console.log("🚀 ~ ProcessTrx ~ transactionProcess ~ paddedProdidrec:", paddedProdidrec)
                    console.log("🚀 ~ ProcessTrx ~ transactionProcess ~ valorRec:", valorRec)
                    let numberFinish = `${data[0].numerorec}${paddedProdidrec}${valorRec}`;
                    console.log("🚀 ~ ProcessTrx ~ transactionProcess ~ numberFinish:", numberFinish)
                    const encodeNumber = await encodeWithPattern(numberFinish);
                    

                    const linkWp = (token.link != null || token.link != '' || token.link != undefined)
                        ? `Para próximas recargas dale clic aqui: ${process.env.REDESLINK_DOMAIN}/${token.link}${encodeNumber}`
                        : '';

                    let messageIn = `${data[0].nomcome}\nPUERTO RICO\n${data[0].telcome}\nRECARGA EN LINEA ${data[0].prodrec}\nTRANSACCION EXITOSA\nNumero:\t\t\t${data[0].numerorec}\nValor:\t\t\t$${data[0].valorrec}\nIVU:\t\t\t\t$0\nTOTAL:\t\t\t$${data[0].valorrec}\nTransacción:\t\t${data[0].idrec}\nFecha:\t\t\t${fechaFormateada}\nRECUERDA SOLICITAR TU RECIBO IVU\n\n${linkWp}`;

                    console.log("🚀 ~ ProcessTrx ~ transactionProcess ~ messageIn:", messageIn)

                    if (client) {
                        if (client.ready) {
                            //const phoneSend = `573232915235@c.us`;
                            const phoneSend = `1${infoRequest.numero}@c.us`;
                            await client.sendMessage(phoneSend, messageIn);
                            console.log(`📩 Mensaje enviado a ${phoneSend}`);
                        } else {
                            console.error(`❌ Cliente ${comeId} aún no está disponible`);
                        }
                    } else {
                        console.error(`❌ Cliente ${comeId} aún no está disponible`);
                    }


                    /*const clientData = await getClient(token.COME_ID);
                    let messageIn = `*${data[0].nomcome}*
                    PUERTO RICO
                    ${data[0].telcome}
                    RECARGA EN LINEA *${data[0].prodrec}*
                    TRANSACCION EXITOSA
                    *Numero:* ${data[0].numerorec}
                    *Valor:* $${data[0].valorrec}
                    *IVU:* $0
                    *TOTAL:* $${data[0].valorrec}
                    *Transacción:* ${data[0].idrec}
                    *Fecha:* ${fechaFormateada}
                    RECUERDA SOLICITAR TU RECIBO IVU`;
                    console.log("🚀 ~ ProcessTrx ~ transactionProcess ~ messageIn:", messageIn)
                    if (clientData) {
                        clientData.sendMessage(`1${infoRequest.numero}@c.us`, messageIn)
                          .then(() => {
                            console.log("🚀 ~ UserService ~ sendWpMsg ~ client.sendMessage ~ msg:", messageIn)
                          })
                          .catch((error) => {
                            console.error('Error sending message:', error);
                          });
                      }*/

                    for (let clave in data) {
                        if (typeof data[clave] === 'object') {
                            data[clave].merchanfee = infoRequest.merchanfee;
                        }
                    }

                    return { status, message, data };
                } else {
                    return { status: false, message: "Carrier error (" + idTransaccion + ")" };
                }
            }
        } catch (error: any) {
            console.log("🚀 ~ ProcessTrx ~ transactionProcess ~ error:", error.message)
            return { status: false, message: "Carrier error - Catch" };
        }

    }
}

async function encodeWithPattern(number: string): Promise<string> {
    const pattern = [3, 1, 4, 2]; // Patrón de reordenamiento
    let result = '';

    // Asegurarse que el número tenga longitud múltiplo del patrón
    while (number.length % pattern.length !== 0) {
        number = '0' + number;
    }

    // Reordenar los dígitos según el patrón
    for (let i = 0; i < number.length; i += pattern.length) {
        const chunk = number.slice(i, i + pattern.length);
        pattern.forEach(p => {
            result += chunk[p - 1];
        });

    }

    return result;
}


const apiv1Service = new Apiv1Service();
export { apiv1Service };

const processTrx = new ProcessTrx();
export { processTrx };
