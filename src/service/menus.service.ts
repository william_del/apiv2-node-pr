import pool from "../config/databasev2";
import { HttpResponse } from "../util/HttpResponse";
import * as MENU from "../models/querys/MENUS";

class MenusService {
    async getMenus(puveCodigo:any, usua:any) {
        console.log("🚀 ~ MenusService ~ getMenus ~ usua:", usua)
        console.log("🚀 ~ MenusService ~ getMenus ~ puveCodigo:", puveCodigo)
        const httpResponse = new HttpResponse();
        let response;
    
        try {

          const verificaReportonly = MENU.q_getReportOnly(usua);
          response = await pool.query(verificaReportonly);

          if (response.rows[0].count > 0) {
            const query = MENU.q_getMenus(puveCodigo,1);
            response = await pool.query(query);
          }else{
            const query = MENU.q_getMenus(puveCodigo,0);
            response = await pool.query(query);
          }
  
          if (response.rows.length > 0) {
            httpResponse.findAll(response.rows);
            return httpResponse;
          }

          httpResponse.emptyRecords();
          return httpResponse;
        } catch (error) {
          console.log(error);
          httpResponse.error(error);
          return httpResponse;
        }
    }

    async getBanner(flag: Number) {
      const httpResponse = new HttpResponse();
      let response;
  
      try {
        const query = MENU.queryGetBanner(flag);
        response = await pool.query(query);
  
        if (response.rows.length > 0) {
          httpResponse.findAll(response.rows);
          return httpResponse;
        }
        httpResponse.emptyRecords();
        return httpResponse;
      } catch (error) {
        console.log(error);
        httpResponse.error(error);
        return httpResponse;  
      }
    }

    async getNotification() {
      const httpResponse = new HttpResponse();
      let response;
  
      try {
        const query = MENU.queryGetNotification();
        response = await pool.query(query);
  
        if (response.rows.length > 0) {
          httpResponse.findAll(response.rows);
          return httpResponse;
        }
        httpResponse.emptyRecords();
        return httpResponse;
      } catch (error) {
        console.log(error);
        httpResponse.error(error);
        return httpResponse;  
      }
      
    }
}

const menusService = new MenusService();
export default menusService;
