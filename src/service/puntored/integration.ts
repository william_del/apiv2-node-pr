import axios from "axios";
import { IRecharge } from "../../models/interfaces/IRecharge";
import { getProductByCode } from "../../util/functionsDb";

class PuntoredService {

    async rechargePuntored( infoRequest: IRecharge, transaccionIn: any){
        console.log("🚀 ~ file: integration.ts ~ PuntoredService ~ rechargePuntored ~ infoRequest:", infoRequest)
        console.log("🚀 ~ file: integration.ts ~ PuntoredService ~ rechargePuntored ~ transaccionIn:", transaccionIn)
        //console.log("🚀 ~ file: integration.ts:7 ~ PuntoredService ~ rechargePuntored ~ infoRequest", infoRequest)
        try {

            let url = process.env.PUNTORED_URL_INTEGRATION || '';
            let pathLogin = process.env.PUNTORED_PATH_LOGIN || ''; 
            let pathQuery = process.env.PUNTORED_PATH_QUERY || '';
            let pathBuy = process.env.PUNTORED_PATH_BUY || '';
            let pathBuyPin = process.env.PUNTORED_PATH_BUY_PACK || '';
            let pathQueryPack = process.env.PUNTORED_PATH_QUERY_PACK || '';
            let pathBuyPack = process.env.PUNTORED_PATH_BUY_PACK || '';
            const username = process.env.PUNTORED_USER || '';
            const password = process.env.PUNTORED_PASS || '';

            let tokenPuntored :any;
            let queryID :any;
            let trxPuntored :any;

            let dataLogin = {
                auth: {
                    username,
                    password,
                }
            };

            //console.log("🚀 ~ file: integration.ts:31 ~ PuntoredService ~ rechargePuntored ~ dataLogin:", url+pathLogin)
            //console.log("🚀 ~ file: integration.ts:31 ~ PuntoredService ~ rechargePuntored ~ dataLogin:", JSON.stringify(dataLogin))
            
            const responseLogin = await axios.post(url + pathLogin, {}, dataLogin);
            console.log("🚀 ~ file: integration.ts:23 ~ PuntoredService ~ response", responseLogin.data);
            tokenPuntored = responseLogin.data.data.access_token;
            const skuCompleted = await getProductByCode(infoRequest.operador);
            //let skuCompleted = infoRequest.operador.toString().padStart(8, "0");
            console.log("🚀 ~ file: integration.ts ~ PuntoredService ~ rechargePuntored ~ skuCompleted:", skuCompleted)
            if (Number(infoRequest.tipo) == 1){
                //let skuCompleted = infoRequest.operador.toString().padStart(8, "0");
                let dataQuery = {
                    productId: `${skuCompleted}`,
                    amount: infoRequest.valor,
                    currencyIdOrigin: 1,
                    currencyIdDestiny: 2,
                    productType: 1,
                };

                let optionsQuery: Object = {
                    method: "POST",
                    url: url+pathQuery,
                    headers: {
                      "Content-Type": "application/json",
                      Authorization: "Bearer " + tokenPuntored,
                    },
                    data: dataQuery,
                };
    
                await axios.request(optionsQuery)
                        .then((response) => {
                            queryID = response.data.data.queryId;
                        })
                        .catch((error) => {
                            queryID = 0;
                            console.log("🚀 ~ file: integration.ts:119 ~ PuntoredService ~ rechargePuntored ~ error:", error)
                        });
                
                console.log("🚀 ~ file: integration.ts ~ PuntoredService ~ .then ~ queryID:", queryID)
                
                let dataBuy = {
                    queryId: queryID,
                    msisdn: infoRequest.numero,
                    trace: transaccionIn,
                };

                console.log("🚀 ~ file: integration.ts ~ PuntoredService ~ rechargePuntored ~ dataBuy:", JSON.stringify(dataBuy))
                
                let optionsBuy: Object = {
                    method: "POST",
                    url: url+pathBuy,
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: "Bearer " + tokenPuntored,
                    },
                    data: dataBuy,
                };
    
                await axios.request(optionsBuy)
                        .then((response) => {
                            trxPuntored = response.data;
                        })
                        .catch((error) => {
                            trxPuntored = error.response.data;
                        });
    
            }

            if (Number(infoRequest.tipo) == 3){
                //let skuCompleted = infoRequest.operador.toString().padStart(8, "0");
                let dataQuery = {
                    productId: `${skuCompleted}`,
                    amount: infoRequest.valor,
                    currencyIdOrigin: 1,
                    currencyIdDestiny: 3,
                    productType: 7,
                };

                let optionsQuery: Object = {
                    method: "POST",
                    url: url+pathQuery,
                    headers: {
                      "Content-Type": "application/json",
                      Authorization: "Bearer " + tokenPuntored,
                    },
                    data: dataQuery,
                };
    
                await axios.request(optionsQuery)
                        .then((response) => {
                            queryID = response.data.data.queryId;
                        })
                        .catch((error) => {
                            queryID = 0;
                            console.log("🚀 ~ file: integration.ts:128 ~ PuntoredService ~ rechargePuntored ~ error:", error)
                        });
                
                console.log("🚀 ~ file: integration.ts ~ PuntoredService ~ .then ~ queryID:", queryID)
                
                let dataBuy = {
                    queryId: queryID,
                    msisdn: infoRequest.numero,
                    trace: transaccionIn,
                    additionalData: "",
                };

                console.log("🚀 ~ file: integration.ts ~ PuntoredService ~ rechargePuntored ~ dataBuy:", JSON.stringify(dataBuy))
                
                let optionsBuy: Object = {
                    method: "POST",
                    url: url+pathBuyPin,
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: "Bearer " + tokenPuntored,
                    },
                    data: dataBuy,
                };
    
                await axios.request(optionsBuy)
                        .then((response) => {
                            trxPuntored = response.data;
                        })
                        .catch((error) => {
                            trxPuntored = error.response.data;
                        });
    
            }
            
            return trxPuntored;
            
        } catch (error) {
            console.log("🚀 ~ file: integration.ts:88 ~ PuntoredService ~ rechargePuntored ~ error", error)
            return 0;
        }  
    }

}

const puntoredService = new PuntoredService();
export default puntoredService;
