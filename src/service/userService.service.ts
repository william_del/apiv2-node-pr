import { ILogin } from "../models/interfaces/ILogin";
import pool from "../config/databasev2";
import { HttpResponse } from "../util/HttpResponse";
import jwt from "jsonwebtoken";
import { IToken } from "../models/interfaces/IToken";
import * as USUARIO from "../models/querys/USUARIO";
import moment from "moment";
import { generarCodigo, sendEmail } from "../util/globalFunctions";
import { template_codigoVerificacion } from "../util/templates/codigoVerificacion";

const fs = require('fs');
const path = require('path');
import { Client, LocalAuth } from 'whatsapp-web.js';
//const { Client, LocalAuth } = require('whatsapp-web.js');
const qrcode = require('qrcode');
//const clients = {};
export const clients: Record<string, any> = {};

class UserService {
  async login(user: ILogin) {
    const httpResponse = new HttpResponse();
    let responseTidi, response, responseLinkCode;
    try {
      responseTidi = await pool.query(USUARIO.queryTipoDisp(user.dispositivo));
      console.log("🚀 ~ UserService ~ login ~ responseTidi:", responseTidi.rows[0].tipodisp)
      if (responseTidi.rows.length > 0) {
        if (responseTidi.rows[0].tipodisp == 6 && user.portal) {
          httpResponse.genericMsgResponse('Su dispositivo no es portal de servicios');
          return httpResponse;
        } else {
          response = await pool.query(USUARIO.login(user.clave, user.dispositivo));
          console.log("🚀 ~ UserService ~ login ~ response:", response.rows[0])
          if (response.rows.length > 0) {

            let date = new Date();
            let cierredias = response.rows[0].diasnohabiles;
            let diasemana = date.getDay();
            diasemana = (diasemana === 0) ? 1 : diasemana + 1;
            let cierrediasBitString = (parseInt(cierredias).toString(2)).padStart(7, '0');
            let diaCierre = cierrediasBitString[diasemana - 1];

            let abre = response.rows[0].abre;
            let cierra = response.rows[0].cierra;

            let currentDate = moment();
            const horaEnFormato = currentDate.format('HH:mm:ss');
            const horaHabil = (moment(horaEnFormato, 'HH:mm:ss').isAfter(moment(abre, 'HH:mm:ss'))) && (moment(horaEnFormato, 'HH:mm:ss').isBefore(moment(cierra, 'HH:mm:ss')));

            const aplicaHoraHabil = response.rows[0].aplicahorario;

            if (response.rows[0].activo == 3) {
              httpResponse.operatorInactive(response.rows[0].INPE_PRIMERNOMBRE + ' ' + response.rows[0].INPE_PRIMERAPELLIDO);
              return httpResponse;
            } else if (diaCierre === '1' || (!horaHabil && aplicaHoraHabil == 1)) {
              httpResponse.genericMsgResponse('Su dispositivo no puede realizar transacciones en este día');
              return httpResponse;
            } else {

              let qrCodeUrl = null;
              /*if (!clients[response.rows[0].COME_ID]) {
                const userId = response.rows[0].COME_ID;
                const clientId = `whatsapp-session-${userId}`;
                // Crear un nuevo cliente de WhatsApp
                const client = new Client({
                  authStrategy: new LocalAuth({ clientId }) // Guarda la sesión
                });

                client.on('qr', (qr) => {
                  console.log('generando qr');
                  qrcode.toDataURL(qr, (err, url) => {
                    if (err) {
                      console.error('Error al generar el código QR:', err);
                      return;
                    }
                    qrCodeUrl = url;
                  });
                });

                client.on('ready', () => {
                  //console.log('Client is ready!');
                  //clients[response.rows[0].COME_ID].ready = true;
                  console.log(`Cliente listo para usuario ${userId}`);
                  clients[userId] = client;
                  clients[userId].ready = true;
                });

                client.initialize();

                while (!qrCodeUrl) {
                  await new Promise((resolve) => setTimeout(resolve, 100)); // Espera 100 ms
                }
              }*/


              response.rows[0].qrCodeUrl = qrCodeUrl;

              const token = await this.generateToken(response.rows[0]);
              httpResponse.findOne({ token });
              return httpResponse;
            }
          } else {
            httpResponse.errorNotClientFound(
              "USUARIOS",
              "dispositivo",
              user.dispositivo
            );
          }
        }
      } else {
        httpResponse.genericMsgResponse('Su dispositivo es valido');
        return httpResponse;
      }
      response = await pool.query(USUARIO.login(user.clave, user.dispositivo));

      httpResponse.errorNotClientFound(
        "USUARIOS",
        "dispositivo",
        user.dispositivo
      );
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async infoUser(token: any) {
    const httpResponse = new HttpResponse();
    let response;
    try {
      const query = USUARIO.getInfo(token.COME_ID, token.dispo, token.clave)
      response = await pool.query(query);
      if (response.rows.length > 0) {
        httpResponse.findAll(response.rows);
        return httpResponse;
      }
      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async loginVerification(token: any, ippub: any, ippriv: any) {
    const httpResponse = new HttpResponse();
    let response, response2;
    const fechaFormateada = moment().format('YYYY-MM-DD HH:mm:ss');
    try {
      const query = USUARIO.queryLoginVerification(token.dispo, token.PUVE_ID, ippub, ippriv)
      response = await pool.query(query);
      if (response.rows.length > 0) {
        const objeto = response.rows[0];
        console.log("🚀 ~ UserService ~ loginVerification ~ objeto:", objeto)
        let cumpleCondicion = objeto && objeto.codigoverificacion_activacionip && objeto.codigoverificacion_activacionip.startsWith('(1');
        console.log("🚀 ~ UserService ~ loginVerification ~ cumpleCondicion:", cumpleCondicion)

        if (cumpleCondicion) {
          httpResponse.findAll({ access: cumpleCondicion });
          return httpResponse;
        }

        const codigo = await generarCodigo();
        console.log("🚀 ~ UserService ~ loginVerification ~ codigo:", codigo)

        const query2 = USUARIO.queryInsertCodigo(token.dispo, token.PUVE_ID, ippub, ippriv, codigo)
        await pool.query(query2);

        const subject = 'Codigo de verificación';
        const msg = 'Favor confirmar su registro con el siguiente codigo:';

        const html = await template_codigoVerificacion(token.dispo, fechaFormateada, msg, codigo);

        await sendEmail(token.comercio_email, subject, html);

        cumpleCondicion = 'Pending';
        httpResponse.findAll({ access: cumpleCondicion });
        return httpResponse;
      }
      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async confirmCodVerification(token: any, code: any, ippub: any, ippriv: any) {
    const httpResponse = new HttpResponse();
    let response;
    const fechaFormateada = moment().format('YYYY-MM-DD HH:mm:ss');
    let cumpleCondicion;
    try {
      const query = USUARIO.queryCodVerification(token.dispo, token.PUVE_ID, ippub, ippriv, code)
      response = await pool.query(query);
      if (response.rows.length > 0) {
        const objeto = response.rows[0];

        console.log(objeto);
        console.log(objeto.estado);

        if (objeto.estado == 0) {
          const query = USUARIO.queryUpdateCodVerification(token.dispo, token.PUVE_ID, ippub, ippriv, code)
          await pool.query(query);
          cumpleCondicion = true;
        } else if (objeto.estado == 1) {
          cumpleCondicion = true;
        } else {
          cumpleCondicion = false;
        }

        httpResponse.findAll({ access: cumpleCondicion });
        return httpResponse;
      }
      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async generateToken(token: IToken) {
    return jwt.sign(token, process.env.SECRET_TOKEN || "SECRET_TOKEN", {
      expiresIn: '24h',
    });
  }

  async createComercio(nombre: any, apellido: any, celular: any, email: any, clave: any) {
    const httpResponse = new HttpResponse();
    let response;

    try {
      const query = USUARIO.createComercio(
        nombre, apellido, celular, email, clave
      );
      response = await pool.query(query);

      if (response.rows.length > 0) {
        httpResponse.findOne(response.rows);
        return httpResponse;
      }

      httpResponse.emptyRecords();
      return httpResponse;

    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async findCajero(user: ILogin) {
    const httpResponse = new HttpResponse();
    let response;
    try {
      const query = USUARIO.findCajero(user.clave, user.dispositivo)
      response = await pool.query(query);
      if (response.rows.length > 0) {
        httpResponse.findAll(response.rows);
        return httpResponse;
      }
      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async changePass(token: any, oldPass: any, newPass: any, confirmPass: any) {
    const httpResponse = new HttpResponse();
    let response;
    try {
      if (newPass == confirmPass && oldPass == token.clave) {
        const query = USUARIO.queryChangePass(token.usuario, newPass)
        response = await pool.query(query);
        httpResponse.update(true, "Updated successfully");
        return httpResponse;
      } else {
        httpResponse.update(false, "Cannot update, verify your old password or new password not match with confirmation new password");
        return httpResponse;
      }

      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async createPreRegister(fname: any, lname: any, cell: any, email: any, nameComer: any, nameLegal: any, address: any, country: any) {
    const httpResponse = new HttpResponse();
    let response: any;
    const today = moment().format('YYYY-MM-DD');
    try {
      const query = USUARIO.queryPreRegister(
        fname, lname, cell, email, nameComer, nameLegal, address, country
      );
      response = await pool.query(query);

      await sendEmail('amartinez@pagatodopr.com', `Pre-Registro ${fname} ${lname} - ${today}`, `<h1> Pre-Registro </h1> <br> ${fname} ${lname} ${cell} ${email} ${nameComer} ${nameLegal} ${address} ${country}`);

      httpResponse.success("Preregistro exitoso", {});
      return httpResponse;

    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async allUserPdvs(token: any) {
    const httpResponse = new HttpResponse();
    let response;
    try {
      const query = USUARIO.queryAllUserPdvs(token.COME_ID)
      response = await pool.query(query);
      if (response.rows.length > 0) {
        httpResponse.findAll(response.rows);
        return httpResponse;
      }
      httpResponse.emptyRecords();
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async inactiveUserPdv(usuario: any, estado: any) {
    const httpResponse = new HttpResponse();
    let response;
    try {
      const query = USUARIO.queryUpdateUserPdv(usuario, estado)
      response = await pool.query(query);
      httpResponse.update(true, "Updated successfully");
      return httpResponse;
    } catch (error) {
      console.log(error);
      httpResponse.error(error);
      return httpResponse;
    }
  }

  async generateSession(comeId: any) {
    try {
        let qrCodeUrl = null;
        const sessionId = `whatsapp-session-${comeId}`;
        console.log("🚀 ~ UserService ~ generateSession ~ sessionId:", sessionId);

        // 🔹 Si el cliente ya está autenticado, retornamos sin generar QR
        if (clients[comeId]?.ready) {
            console.log("🟢 Cliente ya autenticado, no se devuelve QR.");
            return null;
        }

        // 🔹 Si existe pero no está autenticado, lo eliminamos
        if (clients[comeId]) {
            console.log("🔴 Cliente existente sin autenticación, eliminando...");
            await clients[comeId].destroy();
            delete clients[comeId];
        }

        // 🔹 Crear nuevo cliente con autenticación persistente
        const client = new Client({
            authStrategy: new LocalAuth({ clientId: sessionId, dataPath: './sessions' }),
            puppeteer: {
                headless: true,
                //executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
                executablePath: '/usr/bin/chromium-browser',
                args: ['--no-sandbox', '--disable-setuid-sandbox']
            }
        });

        clients[comeId] = client; // Guardamos el cliente en memoria

        // 🔹 Esperar a que se genere el QR o el cliente esté listo
        const qrPromise = new Promise((resolve) => {
            client.once('qr', (qr) => {
                console.log("🚀 ~ UserService ~ generateSession ~ Generando código QR...");
                qrcode.toDataURL(qr, (err, url) => {
                    if (err) {
                        console.error('❌ Error al generar el código QR:', err);
                        resolve(null);
                    } else {
                        qrCodeUrl = url;
                        console.log("✅ Código QR generado correctamente.");
                        resolve(url);
                    }
                });
            });
        });

        const initPromise = new Promise((resolve, reject) => {
            client.once('ready', () => {
                console.log(`✅ Cliente ${comeId} listo después de restauración`);
                clients[comeId].ready = true;
                resolve(null);
            });

            client.once('disconnected', async () => {
                console.log(`❌ Cliente ${comeId} desconectado`);
                await client.destroy();
                delete clients[comeId];
                reject(new Error('Cliente desconectado'));
            });

            setTimeout(() => reject(new Error('Timeout al inicializar cliente')), 30000);
        });

        console.log("🚀 ~ UserService ~ generateSession ~ Iniciando cliente...");
        client.initialize();

        // 🔹 Esperamos QR o que el cliente esté listo
        await Promise.race([qrPromise, initPromise]);

        // 🔹 Retornar QR solo si se generó (sesión nueva)
        console.log(qrCodeUrl ? "🔹 QR devuelto al usuario." : "🟢 Cliente ya autenticado.");
        return qrCodeUrl;

    } catch (error: any) {
        console.log("❌ ~ UserService ~ generateSession ~ error:", error.message);
        return null;
    }
}



  async restoreSession(comeId: any, msg: any, phone: any) {
    try {

      // Recuperamos todos los comeIds de los clientes que están en memoria
      const comeIds = Object.keys(clients);
      console.log("🚀 ~ UserService ~ restoreSession ~ comeIds:", comeIds)

      const sessionIds = [`whatsapp-session-${comeId}`];

      console.log("🚀 ~ UserService ~ sendWpMsg ~ sessionIds:", sessionIds);

      // Intentamos restaurar la sesión para el cliente específico
      let client = clients[comeId];

      // Si no hay cliente en memoria, lo restauramos
      if (!client) {
        console.log(`🔄 Cliente ${comeId} no está en memoria, intentando restaurar...`);

        client = new Client({
          authStrategy: new LocalAuth({ clientId: `whatsapp-session-${comeId}` })
        });

        client.on('ready', () => {
          console.log(`✅ Cliente ${comeId} listo después de restauración`);
          clients[comeId] = client;
          clients[comeId].ready = true;
        });

        client.on('disconnected', () => {
          console.log(`❌ Cliente ${comeId} desconectado`);
          //delete clients[comeId]; // Remueve de memoria
          try {
            delete clients[comeId]; // Remueve de memoria
          } catch (err) {
            console.error(`Error al eliminar cliente ${comeId}:`, err);
          }
        });

        // Inicializamos el cliente y esperamos a que esté listo
        await client.initialize();
      }

      // Ahora que tenemos el cliente restaurado o inicializado, verificamos si está listo
      /*if (client.ready) {

        let messageIn = `I AM GROOT\nPUERTO RICO\n78731223624\nRECARGA EN LINEA 100\nTRANSACCION EXITOSA\nNumero:\t\t\t78731223624\nValor:\t\t\t$100\nIVU:\t\t\t\t$0\nTOTAL:\t\t\t$100\nTransacción:\t\t123456789\nFecha:\t\t\t2025-01-29 10:00:00\nRECUERDA SOLICITAR TU RECIBO IVU`;
        const phoneSend = `${phone}@c.us`;
        await client.sendMessage(phoneSend, msg);
        console.log(`📩 Mensaje enviado a ${phone}`);
      } else {
        console.error(`❌ Cliente ${comeId} aún no está disponible`);
      }*/
    } catch (error: any) {
      console.log("🚀 ~ UserService ~ restoreSession ~ error:", error.message)
    }
  }

}

const userService = new UserService();
export default userService;
