import { HttpResponse } from "../util/HttpResponse";
import fs from "fs";
import path from "path";
import { generateDoc } from "../util/generateDoc";

class HomeService {
  async generatePDF(dispositivo: any, diffDays: any) {
    const httpResponse = new HttpResponse();
    try {
      let nameDoc = `${new Date().getTime()}.pdf`;

      var documento = () => {
        return new Promise(async function (resolve, reject) {
          let fileDoc = fs.createWriteStream(
            path.join(__dirname, "../temp", nameDoc)
          );
          const docum = await generateDoc(fileDoc, dispositivo, diffDays);
          fileDoc.on("finish", () =>
            resolve({
              fileDoc,
              myData: fs.createReadStream(fileDoc.path),
            })
          );
          fileDoc.on("error", reject);
        });
      };
      const { fileDoc, myData }: any = await documento();
      httpResponse.create("Invoice", {});
      return httpResponse;
    } catch (e) {
      console.log(e);
      httpResponse.error(e);
      return httpResponse;
    }
  }
}

const homeService = new HomeService();
export default homeService;
