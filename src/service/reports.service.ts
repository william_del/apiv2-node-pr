import pool from "../config/databasev2";
import { HttpResponse } from "../util/HttpResponse";
import * as REP from "../models/querys/REPORTES";

class ReportsService {

    async statusTrxWithIdExtern(dispId: any, numero: any, valor: any, transaccion: any){
        const httpResponse = new HttpResponse();
        let response;

        try {
            const query = REP.q_statusTrxWithIdExtern(dispId,numero,valor,transaccion);
            response = await pool.query(query);
      
            if (response.rows.length > 0) {
              httpResponse.findOne(response.rows);
              return httpResponse;
            }
      
            httpResponse.emptyRecords();
            return httpResponse;
        } catch (error) {
            console.log(error);
            httpResponse.error(error);
            return httpResponse;
        }
    }
    
    async getReports() {
        const httpResponse = new HttpResponse();
        let response;
    
        try {
          const query = REP.queryGetReports();
          response = await pool.query(query);
    
          if (response.rows.length > 0) {
            httpResponse.findAll(response.rows);
            return httpResponse;
          }
          httpResponse.emptyRecords();
          return httpResponse;
        } catch (error) {
          console.log(error);
          httpResponse.error(error);
          return httpResponse;
        }
    }

    async getInfoReport(idRep: any, pdv: any, fechaIni: any, fechaFin: any) {
        const httpResponse = new HttpResponse();
        let response;
    
        try {
          const query = REP.queryDetailReport(idRep, pdv, fechaIni, fechaFin);
          response = await pool.query(query);
    
          if (response.rows.length > 0) {
            httpResponse.findAll(response.rows);
            return httpResponse;
          }
          httpResponse.emptyRecords();
          return httpResponse;
        } catch (error) {
          console.log(error);
          httpResponse.error(error);
          return httpResponse;
        }
    }
    
    async transactionstatus(type: any, code: any, disp: any, puveid: any,fechaIni: any, fechaFin: any){
      const httpResponse = new HttpResponse();
      let response;

      try {
          const query = REP.q_transactionstatus(type,code,disp,puveid,fechaIni,fechaFin);
          response = await pool.query(query);
    
          if (response.rows.length > 0) {
            httpResponse.findOne(response.rows);
            return httpResponse;
          }
    
          httpResponse.emptyRecords();
          return httpResponse;
      } catch (error) {
          console.log(error);
          httpResponse.error(error);
          return httpResponse;
      }
  }

}



const reportsService = new ReportsService();
export default reportsService;
