import axios from "axios";
import { IRedesLink } from "../../models/interfaces/IRedesLink";
import { HttpResponse } from "../../util/HttpResponse";

class RedesLinkService {

    async creaLink(token: any, dataIn: IRedesLink, token2: any){
        const httpResponse = new HttpResponse();
        let responseApi;
        try {

            let url = process.env.REDESLINK_URL || 'https://pay.pagatodopr.com';
            let pathLogin = process.env.REDESLINK_PATHCREALINK || '/api/link'; 
            let tokenBearer = process.env.REDESLINK_BEARER || ''; 

            let prodAll: any;
            try {
                let urlApiv2 = process.env.APIV1_URL + '/v2/api/productos/list' || process.env.APIV1_URL + '/v2/api/productos/list';
                const params = {
                    locationId: token.PUVE_CODIGO,
                    type: 1,
                    idPais: 227
                };
                const headers = {
                    Authorization: token2
                };
        
                prodAll = await axios.get(urlApiv2, { headers, params });
                //console.log("🚀 ~ file: integration.ts:29 ~ RedesLinkService ~ creaLink ~ prodAll:", prodAll.data)

            } catch (error) {
                //console.error("Hubo un error al realizar la solicitud:", error);
                prodAll = [];
            }

            let objectData = {
                merchant_id: token.COME_ID,
                username: token.dispows,
                password: token.clave,
                merchant_name: token.INPE_PRIMERNOMBRE,
                merchant_address: "SAN LORENZO",
                merchant_phone: "1000000001",
                name: "Todos",
                start_at: "2023-02-18 09:32:42",
                end_at: "2029-12-31 00:00:00",
                data: [{"name":"ATT","id":1,"productos":[{"id":3,"code":"02","tipo":1,"nombre":"ATT RTR","valor":"0","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"}]},{"name":"BOOSTMOBILE","id":2,"productos":[{"id":580,"code":"593","tipo":1,"nombre":"BOOSTMOBILE OPEN RTR","valor":"0","valorMin":"1.00","valorMax":"160","longitud":10,"tipoSub":null,"providerId":85,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"}]},{"name":"BOSSREVOLUTION","id":3,"productos":[{"id":1973,"code":"J57","tipo":1,"nombre":"BOSSREVOLUTION $10","valor":"10","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":1974,"code":"J58","tipo":1,"nombre":"BOSSREVOLUTION $15","valor":"15.00","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":1975,"code":"J59","tipo":1,"nombre":"BOSSREVOLUTION $20","valor":"20","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":1976,"code":"J60","tipo":1,"nombre":"BOSSREVOLUTION $25","valor":"25.00","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":1972,"code":"J56","tipo":1,"nombre":"BOSSREVOLUTION $5","valor":"5.00","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":1977,"code":"J61","tipo":1,"nombre":"BOSSREVOLUTION $50","valor":"50","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"}]},{"name":"CLARO","id":4,"productos":[{"id":2,"code":"01","tipo":1,"nombre":"CLARO","valor":"0","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":11,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"}]},{"name":"H2O","id":5,"productos":[{"id":3413,"code":"K83","tipo":1,"nombre":"H2O $100 12 -MONTH P","valor":"100","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":2,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":368,"code":"604","tipo":1,"nombre":"H2O RTR $35 UNLI","valor":"35.00","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":2,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":367,"code":"603","tipo":1,"nombre":"H2O RTR $40 UNLI","valor":"40","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":2,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":369,"code":"605","tipo":1,"nombre":"H2O RTR $50 UNLI","valor":"50","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":2,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":370,"code":"606","tipo":1,"nombre":"H2O RTR $60 UNLI","valor":"60","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":2,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":3409,"code":"K79","tipo":1,"nombre":"H2O RTR $70 UNLIMITE","valor":"70","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":2,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"}]},{"name":"LIBERTY","id":6,"productos":[{"id":2918,"code":"428","tipo":1,"nombre":"LIBERTY ATT PR RTR","valor":"0","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":2,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"},{"id":511,"code":"369","tipo":1,"nombre":"LIBERTY PREPAID RTR","valor":"0","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":85,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"}]},{"name":"SIMPLEMOBILE","id":7,"productos":[{"id":3339,"code":"K09","tipo":1,"nombre":"SIMPLEMOBILE 10-100","valor":"0","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":2,"providerId":84,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"}]},{"name":"T-MOBILE","id":8,"productos":[{"id":523,"code":"381","tipo":1,"nombre":"T-MOBILE","valor":"0","valorMin":"1.00","valorMax":"100","longitud":10,"tipoSub":1,"providerId":85,"sku":null,"skurel":null,"country":"","needsPin":false,"fee":"0","paisId":227,"paisNombre":"PuertoRico - USA"}]}]

            };

            //console.log("🚀 ~ file: integration.ts:49 ~ RedesLinkService ~ creaLink ~ objectData:", JSON.stringify(objectData));

            let ObjectPost: Object = {
                method: "POST",
                url: url+pathLogin,
                headers: {
                  "Content-Type": "application/json",
                  Authorization: "Bearer " + tokenBearer,
                },
                data: objectData,
            };
            console.log("🚀 ~ file: integration.ts:60 ~ RedesLinkService ~ creaLink ~ ObjectPost:", ObjectPost)

            try {
                const responseApiLink = await axios.request(ObjectPost);
                console.log("🚀 ~ file: integration.ts:43 ~ RedesLinkService ~ creaLink ~ responseApiLink:", responseApiLink.data);
                responseApi = responseApiLink.data;
            } catch (error) {
                console.log("🚀 ~ file: integration.ts:56 ~ PuntoredService ~ .then ~ error", error);
            }

            httpResponse.findOne(responseApi);
            return httpResponse;
        } catch (error) {
            console.log("🚀 ~ file: integration.ts:9 ~ RedesLinkService ~ rechargePuntored ~ error", error);
            httpResponse.error(error);
            return httpResponse;
        }
            
    }

    async creaLinkMs(token: any){
        const httpResponse = new HttpResponse();
        let responseApi;
        try {

            let obj = {
                merchant_id: token.COME_ID,
                username: token.dispows,
                password: token.clave,
                merchant_name: token.INPE_PRIMERNOMBRE,
                puve_id: token.PUVE_ID,
                puve_codigo: token.PUVE_CODIGO,
                type_product: '1'
            }

            const responseAxios = await axios.post(`${process.env.REDESLINK_URL_MS}/v2/links/create`, obj);
            console.log("🚀 ~ RedesLinkService ~ creaLinkMs ~ responseAxios:", responseAxios.data);
            responseApi = responseAxios.data.data;

            //httpResponse.findOne( {code: responseApi});
            httpResponse.findOne( responseApi );
            return httpResponse;
        } catch (error) {
            console.log("🚀 ~ file: integration.ts:9 ~ RedesLinkService ~ rechargePuntored ~ error", error);
            httpResponse.error(error);
            return httpResponse;
        }
            
    }

}

const redesLinkService = new RedesLinkService();
export default redesLinkService;