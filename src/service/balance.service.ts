import { IBalance } from "../models/interfaces/IBalance";
import pool from "../config/databasev2";
import { HttpResponse } from "../util/HttpResponse";
import * as BALANCE from "../models/querys/BALANCE";
import { getRepository } from "typeorm";
import { TransaccionBalance } from "../models/entities/transaccionbalance.entity";
import axios from "axios";
class BalanceService {

    /*async rechargeBalancePreviewTest(token: any){
        const httpResponse = new HttpResponse();
        let response;
        try {
            const query = BALANCE.recBalancePreviewTest()
            response = await pool.query(query);
            if (response.rows.length > 0) {
                httpResponse.findAll(response.rows);
                return httpResponse;
            }
            httpResponse.emptyRecords();
            return httpResponse;
        } catch (error) {
            console.log(error);
            httpResponse.error(error);
            return httpResponse;
        }
    }*/

    async rechargeBalancePreview(come: any, valor: any){
        const httpResponse = new HttpResponse();
        try {
            let obj = {
                come: come,
                valor: valor
            }
            const responseAxios = await axios.post(`http://localhost:3004/balance/preview`, obj);
            console.log("🚀 ~ BalanceService ~ rechargeBalancePreview ~ responseAxios:", responseAxios.data);
            return responseAxios.data;
        } catch (error) {
            console.log(error);
            httpResponse.error(error);
            return httpResponse;
        }
    }

    async rechargeBalance(token: any, balance: IBalance){
        const httpResponse = new HttpResponse();
        let response;
        try {

            if(balance.estado === 'COMPLETED'){
                const query = BALANCE.recBalance(token.COME_ID, balance.valor, balance.pasarela, 1, balance.idtrxwebhook, balance.estado)
                response = await pool.query(query);
                if (response.rows.length > 0) {
                    httpResponse.findAll(response.rows);
                    return httpResponse;
                }
                httpResponse.emptyRecords();
                return httpResponse;
            }else{
                let obj = {
                    subtotal: balance.subtotal,
                    tax: balance.tax,
                    total: balance.total,
                    idUniqueWebhook: balance.idtrxwebhook,
                    phoneNumberPay: balance.phonePay,
                    come: token.COME_ID
                }
                console.log("🚀 ~ BalanceService ~ rechargeBalance ~ obj:", obj)
                
                const responseAxios = await axios.post(`http://localhost:3004/balance/athm`, obj);
                console.log("🚀 ~ BalanceService ~ rechargeBalance ~ responseAxios:", responseAxios.data);
                return responseAxios.data;
            }

            /**/
        } catch (error) {
            console.log(error);
            httpResponse.error(error);
            return httpResponse;
        }
    }

    async transferBalance(token: any, balance: IBalance){
        const httpResponse = new HttpResponse();
        let response;
        try {
            const query = BALANCE.traBalance( balance.valor, balance.celular, token.COME_ID, balance.pasarela)
            response = await pool.query(query);
            if (response.rows.length > 0) {
                httpResponse.findAll(response.rows);
                return httpResponse;
            }
            httpResponse.emptyRecords();
            return httpResponse;
        } catch (error) {
            console.log(error);
            httpResponse.error(error);
            return httpResponse;
        }
    }

    async webhookAthm(uniqueId: any, trxIdAth: any, statusAth: any, valueAth: any){
        const httpResponse = new HttpResponse();
        let response: any;
        let comercio: any;
        try {
            const queryRec = BALANCE.recBalance(comercio, valueAth, 5, 2, uniqueId, statusAth);
            response = await pool.query(queryRec);
            console.log(response);
            /*const queryGet = BALANCE.query_get_trxbalance(uniqueId)
            response = await pool.query(queryGet);
            if (response.rows.length > 0) {
                comercio = response.rows[0].COME_ID;
                if(response.rows[0].STATUS === 0){
                    //const queryUpdate = BALANCE.query_update_trxbalance(uniqueId,trxIdAth,statusAth)
                    //response = await pool.query(queryUpdate);
                    console.log('Status: ',statusAth,' ',process.env.STATUS_ATHM_COMPLETED);
                    if(statusAth==='COMPLETED'){
                        console.log('Pago listo para aplicarse');
                        const queryRec = BALANCE.recBalance(comercio, valueAth, 5, 2, statusAth)
                        response = await pool.query(queryRec);
                    }
                }
            }*/
            httpResponse.emptyRecords();
            return httpResponse;
        } catch (error) {
            console.log(error);
            httpResponse.error(error);
            return httpResponse;
        }
    }
}

const balanceService = new BalanceService();
export default balanceService;
