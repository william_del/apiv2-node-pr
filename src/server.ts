import express from "express";
import morgan from "morgan";
import cors from "cors";
import helmet from "helmet";
import dotenv from "dotenv";
import useragent from "express-useragent";
import compression from "compression";
import database from "./config/database";
import pool from "./config/databasev2";

/// Routes

import routes from "./controller/index";
import { errorResourceNotFound } from "./util/errorhandler";

export default class Server {
  public app: express.Application;
  public port: number;

  constructor(port: number) {
    this.app = express();
    this.port = port;
    this.config();
    this.routes();
  }

  static init(port: number): Server {
    return new Server(port);
  }

  config() {
    dotenv.config();
    console.log(`Connecting to pool ${pool.options.port}`);
    // try {
    //   database.connectionDB();
    // } catch (e) {
    //   console.log(e);
    // }
    this.app.use(useragent.express());
    this.app.set("port", process.env.PORT || this.port || 3000);
    this.app.use(morgan("dev"));
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(cors());
    this.app.use((req, res, next) => {
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH, DELETE"
      );
      res.setHeader(
        "Access-Control-Allow-Headers",
        "X-Requested-With,content-type"
      );
      next();
    });
    this.app.use(helmet());
    this.app.use(compression());
  }

  routes(): void {
    this.app.use("/api/test", routes.homeController);
    this.app.use("/api/user", routes.userController);
    this.app.use("/api/productos", routes.productsController);
    this.app.use("/api/balance", routes.balanceController);
    this.app.use("/api/reports", routes.reportsController);
    this.app.use("/api/menus", routes.menusController);

    this.app.use("/api/auth", routes.authController);
    this.app.use(errorResourceNotFound);
  }

  start(): void {
    console.log("✔️ Sesiones restauradas correctamente.");
    this.app.listen(this.app.get("port"), () => {
      console.log("Server listening on port", this.app.get("port"));
    });
  }
}
