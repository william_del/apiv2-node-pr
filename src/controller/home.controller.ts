import { Router, Request, Response } from "express";
import homeService from "../service/home.service";
import routes from "./index";

class HomeController {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  async generatePDF(req: Request, res: Response) {
    const { dispositivo, diffDays } = req.body;
    const { status, message, data } = await homeService.generatePDF(
      dispositivo,
      diffDays
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  routes() {
    this.router.get("/", (req: Request, res: Response) => {
      res.json({
        message: `WELCOME TO PR BACK IN ${process.env.NODE_ENV?.toUpperCase()}`,
      });
    });
    this.router.post("/generate-pdf", this.generatePDF);
  }
}

const homeController = new HomeController();
export default homeController.router;
