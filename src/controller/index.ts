import homeController from "./home.controller";
import userController from "./user.controller";
import productsController from "./products.controller";
import balanceController from "./balance.controller";
import reportsController from "./reports.controller";
import menusController from "./menus.controller";
import authController from "./auth.controller";
const routes = {
  homeController,
  userController,
  productsController,
  balanceController,
  reportsController,
  menusController,
  authController
};
export default routes;
