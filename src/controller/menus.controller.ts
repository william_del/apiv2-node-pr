import { Router, Request, Response, response } from "express";
import { verifyToken } from "../middlewares/verifyToken";
import menusService from "../service/menus.service";

class MenusController {
    router: Router;
  
    constructor() {
      this.router = Router();
      this.routes();
    }

    async getMenus(req: Request, res: Response) {
      const token = req.body.payload;
      const { status, message, data } = await menusService.getMenus(token.PUVE_CODIGO,token.usuario);
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    }

    async getBanner(req: Request, res: Response){
      const flag = req.params.flag;
      const { status, message, data } = await menusService.getBanner(Number(flag));
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    }

    async getNotification(req: Request, res: Response){
      const { status, message, data } = await menusService.getNotification();
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    }

    routes() {
        this.router.get("/getmenus", verifyToken, this.getMenus);
        this.router.get("/getbanner/:flag", this.getBanner);
        this.router.get("/getnotification", this.getNotification);
    }
}

const menusController = new MenusController();
export default menusController.router;