import { Router, Request, Response } from "express";
import { verifyToken } from "../middlewares/verifyToken";
import reportsService from "../service/reports.service";

class ReportsController {
    router: Router;
  
    constructor() {
      this.router = Router();
      this.routes();
    }

    async statusTrxWithIdExtern(req: Request, res: Response) {
        //const token = req.body.payload;
        const { dispId, numero, valor, transaccion } = req.query;

        if (dispId && numero && transaccion) {
            const { status, message, data } =
              await reportsService.statusTrxWithIdExtern(
                dispId,
                numero,
                valor,
                transaccion
              );
            status
              ? res.status(200).json({ status , message, data} )
              : res.status(202).json({ status, message });
          } else {
            res.status(202).json({ status: false, message: "Not valid params" });
          }
    }

    async getReports(_req: Request, res: Response) {
      const { status, message, data } = await reportsService.getReports();
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    }

    async getInfoReport(req: Request, res: Response) {
      const token = req.body.payload;
      const { reporte, fechaIni, fechaFin } = req.body;
      const { status, message, data } = await reportsService.getInfoReport(reporte, token.PUVE_ID, fechaIni, fechaFin);
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    }

    async transactionStatus(req: Request, res: Response) {
      const token = req.body.payload;
      const { type, code, fechaIni, fechaFin } = req.body;

      const { status, message, data } = await reportsService.transactionstatus(type, code, token.dispo, token.PUVE_ID,fechaIni, fechaFin);
      status
            ? res.status(200).json({ status , message, data} )
            : res.status(202).json({ status, message });
  }
  
    routes() {
      this.router.get("/status", verifyToken, this.statusTrxWithIdExtern);
      //this.router.post("/transfer-balance", verifyToken, this.transferBalance);
      this.router.get("/getreports", verifyToken, this.getReports);
      this.router.post("/getinforeport", verifyToken, this.getInfoReport);
      this.router.post("/transaction-status", verifyToken, this.transactionStatus);
    }
  }
  
  const reportsController = new ReportsController();
  export default reportsController.router;