import { Router, Request, Response, response } from "express";
import { verifyToken } from "../middlewares/verifyToken";
import productsService from "../service/products.service";
import { IRecharge } from "../models/interfaces/IRecharge";
//import { existeTrx } from "../models/querys/TRANSACCION";
import puntoredService from "../service/puntored/integration";
import { apiv1Service, processTrx } from "../service/apiv1/integration";
import { IRedesLink } from "../models/interfaces/IRedesLink";
import redesLinkService from "../service/redeslink/integration";
import csqService from "../service/csq/integration";
const axios = require("axios");

class ProductsController {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  async getInfoProductos(req: Request, res: Response) {
    const token = req.body.payload;
    const idPais = req.params.idP;
    const idTipoP = req.params.idTP;

    if (idPais && idTipoP) {
      const { status, message, data } = await productsService.getInfoProductos(
        token,
        idPais,
        idTipoP
      );
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    } else {
      res.status(202).json({ status: false, message: "Not valid params" });
    }
  }

  async getInfoProductosOldVersion(req: Request, res: Response) {
    const token = req.body.payload;
    const { idPais, locationId, type } = req.query;

    if (locationId && type) {
      const { status, message, data } =
        await productsService.getInfoProductosOldVersion(
          token,
          locationId,
          type,
          idPais
        );
      status
        ? res.status(200).json(data)
        : res.status(202).json({ status, message });
    } else {
      res.status(202).json({ status: false, message: "Not valid params" });
    }
  }

  async getInfoPaises(req: Request, res: Response) {
    const token = req.body.payload;
    const idTipoP = req.params.idTP;

    console.log(idTipoP);
    const { status, message, data } = await productsService.getInfoPaises(
      token,
      idTipoP
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async getInfoPaisesSinIdTp(req: Request, res: Response) {
    const token = req.body.payload;

    const { status, message, data } = await productsService.getInfoPaises(
      token,
      1
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async recharge(req: Request, res: Response) {
    const version = req.headers['version'] || '1.2.1';
    const token = req.body.payload;
    const infoRequest: IRecharge = req.body;
    let idTransaccionPuntored: any;
    let idTransaccionCsq: any;
    let idTransaccion:any;
    let idTransaccionProcesar:any;

    if (infoRequest.password === null || infoRequest.password === undefined || infoRequest.password === '') {
      infoRequest.password = req.body.payload.clave;
    }

    console.log(">>>>>>>>>>>>>>>>>>>>><INICIA RECARGA", infoRequest)

    /*let countTrx = await productsService.existeTrx(
      token,
      infoRequest.numero,
      infoRequest.valor
    );
    console.log("🚀 ~ countTrx:", countTrx)*/

    if (true) {

        if(infoRequest.proveedor == process.env.PROV_PUNTORED){
          console.log("🚀 ~ file: products.controller.ts ~ ProductsController ~ recharge ~ PROV_PUNTORED:::::::::::INICIO")
          
          idTransaccionProcesar = await productsService.generateTrx(token, infoRequest);
  
          await puntoredService.rechargePuntored(infoRequest, idTransaccionProcesar).then((response) => {
            idTransaccionPuntored = response;
          });
  
          console.log("🚀 ~ file: products.controller.ts ~ awaitpuntoredService.rechargePuntored ~ response:", idTransaccionPuntored)

          if(idTransaccionPuntored.code != "00"){
            if (idTransaccionPuntored.data == null) {
              idTransaccionPuntored.data = {
                code: '99',
                transactionId: idTransaccionPuntored.message
              };
            }
            if (idTransaccionPuntored.data.transactionId == null) {
              idTransaccionPuntored.data.transactionId = idTransaccionPuntored.data.message;
            }
            //idTransaccionPuntored.data.code = "99";
            console.log("🚀 ~ recharge ~ idTransaccionPuntored:", idTransaccionPuntored)
            await productsService.updateTrx(token, infoRequest, idTransaccionProcesar, idTransaccionPuntored.data);
            await productsService.updateTrxVersion(idTransaccionProcesar, version);
            res.status(202).json({ status: false, message: "Carrier error" });
            return;
          }

          idTransaccionPuntored.data.code = "00";
          await productsService.updateTrx(token, infoRequest, idTransaccionProcesar, idTransaccionPuntored.data);
          await productsService.updateTrxVersion(idTransaccionProcesar, version);
  
          idTransaccion =  idTransaccionProcesar;
  
          const { status, message, data } = await productsService.getInfoTrx(
            idTransaccion
          );

          status ? res.status(200).json({ status, message, data }): res.status(202).json({ status, message });
  
          console.log("🚀 ~ file: products.controller.ts ~ ProductsController ~ recharge ~ PROV_PUNTORED:::::::::::FIN") 
  
        }else if(infoRequest.proveedor == process.env.PROV_CSQ){
          console.log("🚀 ~ file: products.controller.ts ~ ProductsController ~ recharge ~ PROV_CSQ:::::::::::INICIO")
          idTransaccionProcesar = await productsService.generateTrx(token, infoRequest);
          console.log("🚀 ~ file: products.controller.ts:151 ~ idTransaccionProcesar:", idTransaccionProcesar)
  
          if(idTransaccionProcesar==null){
            res.status(202).json({ status: false, message: "Carrier error" });
            return;
          }

          await csqService.rechagueCsq(infoRequest, idTransaccionProcesar).then((response) => {
            idTransaccionCsq = response;
          });
  
          console.log("🚀 ~ file: products.controller.ts:140 ~ awaitcsqService.rechagueCsq ~ idTransaccionCsq:", idTransaccionCsq)
  
          if (idTransaccionCsq && idTransaccionCsq.rc === 0 && idTransaccionCsq.items && idTransaccionCsq.items.length > 0) {
            idTransaccionCsq.code = "00";
            idTransaccionCsq.transactionId = idTransaccionCsq.items[0].supplierreference;
          } else {
              idTransaccionCsq.code = "99";
              // Verificar si idTransaccionCsq.items es un array no vacío antes de acceder al primer elemento
              idTransaccionCsq.transactionId = Array.isArray(idTransaccionCsq.items) && idTransaccionCsq.items.length > 0
                  ? idTransaccionCsq.items[0].resultcode
                  : 'Error controlado';
          }
  
          //console.log("🚀 ~ file: products.controller.ts:131 ~ ProductsController ~ recharge ~ idTransaccionCsq:", idTransaccionCsq)
  
          await productsService.updateTrx(token, infoRequest, idTransaccionProcesar, idTransaccionCsq);
          await productsService.updateTrxVersion(idTransaccionProcesar, version);
  
          if(idTransaccionCsq.code == "99"){
            res.status(202).json({ status: false, message: "Carrier error" });
            return;
          }
  
          idTransaccion =  idTransaccionProcesar;
          const { status, message, data } = await productsService.getInfoTrx(
            idTransaccion
          );
          status ? res.status(200).json({ status, message, data }): res.status(202).json({ status, message });
        }else{
          console.log("🚀 ~ file: products.controller.ts:146 ~ ProductsController ~ recharge ~ pr:")
          const resultTransaction = await processTrx.transactionProcess(token, infoRequest, version);

          resultTransaction?.status
            ? res.status(200).json( resultTransaction )
            : res.status(202).json( resultTransaction );
        }


      console.log(">>>>>>>>>>>>>>>>>>>>>FIN RECARGA", infoRequest)
    } else {
      res.status(202).json({
        status: false,
        message: "El número celular " + infoRequest.numero + " ya fue procesado",
      });
    }
  }

  async giftcardProcess(req: Request, res: Response) {
    const version = req.headers['version'] || '1.2.1';
    const token = req.body.payload;
    const infoRequest: IRecharge = req.body;

    if (infoRequest.password === null || infoRequest.password === undefined || infoRequest.password === '') {
      infoRequest.password = req.body.payload.clave;
    }

    console.log(">>>>>>>>>>>>>>>>>>>>><INICIA GIFTCARD", infoRequest);

    if(infoRequest.proveedor == process.env.PROV_CSQ){
      console.log("🚀 ~ file: products.controller.ts ~ ProductsController ~ recharge ~ PROV_CSQ:::::::::::INICIO")
      const resultTransaction = await productsService.processCSQ(token, infoRequest, version);

      resultTransaction?.status
            ? res.status(200).json( resultTransaction )
            : res.status(202).json( resultTransaction );
    }

    
  }

  async rechargeEvertec(req: Request, res: Response) {
    const token = req.body.payload;
    const infoRequest: IRecharge = req.body;
    let transaccion = req.query.transaccion;
    let idTransaccion;

    console.log(transaccion);

    let countTrx = await productsService.existeTrx(
      token,
      infoRequest.numero,
      infoRequest.valor
    );
    if (countTrx == 0) {
      await apiv1Service.rechargeOldApi(token, infoRequest, transaccion).then((response) => {
        idTransaccion = response;
      });
      
      //console.log('antes: ', idTransaccion);
      idTransaccion = transaccion === undefined ? 1 + idTransaccion : Number(idTransaccion);
      //console.log('despues: ', idTransaccion);

      const { status, message, data } = await productsService.getInfoTrx(
        idTransaccion
      );

      console.log(transaccion);

      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
      /*if (transaccion === undefined){
        status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
      }else{
        let data2 = {
          message: null,
          idTransaccion: idTransaccion,
          estado: true
        };
        status
        ? res.status(200).json(data2)
        : res.status(202).json({ status, message });
      }*/
      
    } else {
      res.status(202).json({
        status: false,
        message:
          "El número celular " + infoRequest.numero + " ya fue procesado",
      });
    }
  }

  async getMovements(req: Request, res: Response) {
    const token = req.body.payload;
    const { fechaIni, fechaFin } = req.body;

    //console.log("fechas: ",fechaIni," ",fechaFin.length);

    if (fechaIni.length > 0 && fechaFin.length > 0) {
      const { status, message, data } = await productsService.getMovimientos(
        token,
        fechaIni,
        fechaFin
      );
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    } else {
      console.log("Una esta vacia");
    }
  }

  async getTopFive(req: Request, res: Response) {
    const token = req.body.payload;

    if (token) {
      const { status, message, data } = await productsService.getTopFive(
        token
      );
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    } else {
      console.log("Una esta vacia");
    }
  }

  async getCountry(_req: Request, res: Response) {
    const { status, message, data } = await productsService.getCountry();
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async verifyPass(req: Request, res: Response) {
    const token = req.body.payload;
    const clave = req.body.clave;
    //console.log(clave);
    const { status, message, data } = await productsService.verifyPass(
      token,
      clave
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async creaRedesLink(req: Request, res: Response) {
    const token = req.body.payload;
    const tokenNocodificaed = req.headers.authorization;
    const dataIn: IRedesLink = req.body;

    const { status, message, data } = await redesLinkService.creaLink(
      token,
      dataIn,
      tokenNocodificaed
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async creaRedesLinkMs(req: Request, res: Response) {
    const token = req.body.payload;

    const { status, message, data } = await redesLinkService.creaLinkMs(
      token
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async processPin(req: Request, res: Response) {
    const token = req.body.payload;
    const infoRequest: IRecharge = req.body;
    let idTransaccionProcesar: any;
    let idTransaccionPuntored: any;
    let idTransaccion: any;

    if(infoRequest.proveedor == process.env.PROV_PUNTORED){
      idTransaccionProcesar = await productsService.generateTrx(token, infoRequest);
      console.log("🚀 ~ file: products.controller.ts:337 ~ processPin ~ idTransaccionProcesar:", idTransaccionProcesar)

      await puntoredService.rechargePuntored(infoRequest, idTransaccionProcesar).then((response) => {
        idTransaccionPuntored = response;
        //console.log("🚀 ~ file: products.controller.ts:108 ~ ProductsController ~ awaitpuntoredService.rechargePuntored ~ idTransaccionPuntored:", idTransaccionPuntored)
      });

      if(idTransaccionPuntored==0){
        idTransaccionPuntored.code = "99";
        idTransaccionPuntored.transactionId = "Error";
      }
      //if(idTransaccionPuntored.message == 'BALANCE NOT AVAILABLE'){
      if(idTransaccionPuntored.code == 'HE11'){
        idTransaccionPuntored.code = "99";
        idTransaccionPuntored.transactionId = "Error";
      }
      await productsService.updateTrx(token, infoRequest, idTransaccionProcesar, idTransaccionPuntored);

      idTransaccion =  idTransaccionProcesar;

      if(idTransaccionProcesar.code == "99"){
        res.status(202).json({ status: false, message: "Carrier error" });
      }

      const { status, message, data } = await productsService.getInfoTrx(
        idTransaccion
      );
      status ? res.status(200).json({ status, message, data }): res.status(202).json({ status, message });
    }else{
      const responseProcessPin = await apiv1Service.pinOldApi(token, infoRequest, undefined);
      console.log("🚀 ~ file: products.controller.ts:265 ~ ProductsController ~ processPin ~ responseProcessPin:", responseProcessPin);
  
      const { status, message, data } = await productsService.getInfoTrPinManual(
        responseProcessPin, infoRequest
      );
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
    }
  }

  async saveMerchantfee(req: Request, res: Response) {
    const token = req.body.payload;
    const valor = req.body.valor;
    //console.log(clave);
    const { status, message, data } = await productsService.saveMerchantfee(
      token,
      valor
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async getInfoLink(req: Request, res: Response) {
    const codelink = req.params.codelink;

    let objectData;
    if (codelink == "william"){
      objectData = {
        merchant_id: 2422,
        username: 2345,
        password: "Desarrollo123*",
        merchant_name: "IM GROOT",
        merchant_address: "SAN LORENZO",
        merchant_phone: "1000000001",
        banner: "https://pay.pagatodopr.com/storage/merchants/April2020/Zt91TVgeqOTC67gKxlxY.png",
        data: [
          {
            "name": "ATT",
            "id": 1,
            "logo": "https://pay.pagatodopr.com/img/att.png",
            "productos": [
              {
                "id": 3,
                "code": "02",
                "tipo": 1,
                "nombre": "ATT RTR",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "BOOSTMOBILE",
            "logo": "https://pay.pagatodopr.com/img/boostmobile.png",
            "id": 2,
            "productos": [
              {
                "id": 580,
                "code": "593",
                "tipo": 1,
                "nombre": "BOOSTMOBILE OPEN RTR",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "160",
                "longitud": 10,
                "tipoSub": null,
                "providerId": 85,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "BOSSREVOLUTION",
            "logo": "https://pay.pagatodopr.com/storage/merchants/April2020/Zt91TVgeqOTC67gKxlxY.png",
            "id": 3,
            "productos": [
              {
                "id": 1973,
                "code": "J57",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $10",
                "valor": "10",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1974,
                "code": "J58",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $15",
                "valor": "15.00",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1975,
                "code": "J59",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $20",
                "valor": "20",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1976,
                "code": "J60",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $25",
                "valor": "25.00",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1972,
                "code": "J56",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $5",
                "valor": "5.00",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1977,
                "code": "J61",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $50",
                "valor": "50",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "CLARO",
            "id": 4,
            "logo": "https://pay.pagatodopr.com/img/claro.png",
            "productos": [
              {
                "id": 2,
                "code": "01",
                "tipo": 1,
                "nombre": "CLARO",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 11,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "H2O",
            "id": 5,
            "logo": "https://pay.pagatodopr.com/img/h2o.png",
            "productos": [
              {
                "id": 3413,
                "code": "K83",
                "tipo": 1,
                "nombre": "H2O $100 12 -MONTH P",
                "valor": "100",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 2,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 368,
                "code": "604",
                "tipo": 1,
                "nombre": "H2O RTR $35 UNLI",
                "valor": "35.00",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 2,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 367,
                "code": "603",
                "tipo": 1,
                "nombre": "H2O RTR $40 UNLI",
                "valor": "40",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 2,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 369,
                "code": "605",
                "tipo": 1,
                "nombre": "H2O RTR $50 UNLI",
                "valor": "50",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 2,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 370,
                "code": "606",
                "tipo": 1,
                "nombre": "H2O RTR $60 UNLI",
                "valor": "60",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 2,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 3409,
                "code": "K79",
                "tipo": 1,
                "nombre": "H2O RTR $70 UNLIMITE",
                "valor": "70",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 2,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "LIBERTY",
            "id": 6,
            "logo": "",
            "productos": [
              {
                "id": 2918,
                "code": "428",
                "tipo": 1,
                "nombre": "LIBERTY ATT PR RTR",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 2,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 511,
                "code": "369",
                "tipo": 1,
                "nombre": "LIBERTY PREPAID RTR",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 85,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "SIMPLEMOBILE",
            "id": 7,
            "logo": "https://pay.pagatodopr.com/img/simplemobile.png",
            "productos": [
              {
                "id": 3339,
                "code": "K09",
                "tipo": 1,
                "nombre": "SIMPLEMOBILE 10-100",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 2,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "T-MOBILE",
            "id": 8,
            "logo": "https://pay.pagatodopr.com/img/t-mobile.png",
            "productos": [
              {
                "id": 523,
                "code": "381",
                "tipo": 1,
                "nombre": "T-MOBILE",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 85,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          }
        ]
      }
    }
    if (codelink == "caleb"){
      objectData = {
        merchant_id: 2422,
        username: 2345,
        password: "123456",
        merchant_name: "IM GROOT",
        merchant_address: "SAN LORENZO",
        merchant_phone: "1000000001",
        banner: "https://t3.ftcdn.net/jpg/02/68/48/86/360_F_268488616_wcoB2JnGbOD2u3bpn2GPmu0KJQ4Ah66T.jpg",
        data: [
          {
            "name": "ATT",
            "id": 1,
            "logo": "https://pay.pagatodopr.com/img/att.png",
            "productos": [
              {
                "id": 3,
                "code": "02",
                "tipo": 1,
                "nombre": "ATT RTR",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "BOOSTMOBILE",
            "logo": "https://pay.pagatodopr.com/img/boostmobile.png",
            "id": 2,
            "productos": [
              {
                "id": 580,
                "code": "593",
                "tipo": 1,
                "nombre": "BOOSTMOBILE OPEN RTR",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "160",
                "longitud": 10,
                "tipoSub": null,
                "providerId": 85,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "BOSSREVOLUTION",
            "logo": "https://pay.pagatodopr.com/storage/merchants/April2020/Zt91TVgeqOTC67gKxlxY.png",
            "id": 3,
            "productos": [
              {
                "id": 1973,
                "code": "J57",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $10",
                "valor": "10",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1974,
                "code": "J58",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $15",
                "valor": "15.00",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1975,
                "code": "J59",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $20",
                "valor": "20",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1976,
                "code": "J60",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $25",
                "valor": "25.00",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1972,
                "code": "J56",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $5",
                "valor": "5.00",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              },
              {
                "id": 1977,
                "code": "J61",
                "tipo": 1,
                "nombre": "BOSSREVOLUTION $50",
                "valor": "50",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 84,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          },
          {
            "name": "CLARO",
            "id": 4,
            "logo": "https://pay.pagatodopr.com/img/claro.png",
            "productos": [
              {
                "id": 2,
                "code": "01",
                "tipo": 1,
                "nombre": "CLARO",
                "valor": "0",
                "valorMin": "1.00",
                "valorMax": "100",
                "longitud": 10,
                "tipoSub": 1,
                "providerId": 11,
                "sku": null,
                "skurel": null,
                "country": "",
                "needsPin": false,
                "fee": "0",
                "paisId": 227,
                "paisNombre": "PuertoRico - USA"
              }
            ]
          }
        ]
      }
    }
    console.log("🚀 ~ getInfoLink ~ objectData:", objectData)
    try {
      if (!objectData){
        res.status(400).json({ status: true, message: "Code not found", data: [] })
      }else{
        res.status(200).json({ status: true, message: "OK", data: objectData })
      }
    } catch (error) {
      console.log("🚀 ~ getInfoLink ~ error:", error)
      
    } 
  }

  

  //#region CSQ
  async validateDoc(req: Request, res: Response){
    const doc = req.params.doc;

    const { status, message, data } = await productsService.validateDoc(doc);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async validatePhone(req: Request, res: Response){
    const num = req.params.num;

    const { status, message, data } = await productsService.validatePhone(num);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async registerSM(req: Request, res: Response){
    
    const body = req.body;

    const { status, message, data } = await productsService.registerSM(body);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async saleSM(req: Request, res: Response){
    const token = req.body.payload;
    const infoRequest = req.body;

    const { status, message, data } = await productsService.saleSM(token, infoRequest);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }
  //#endregion

  routes() {
    this.router.get("/info-productos/:idTP/:idP",verifyToken,this.getInfoProductos);
    this.router.get("/info-paises/:idTP", verifyToken, this.getInfoPaises);
    this.router.get("/info-paises", verifyToken, this.getInfoPaisesSinIdTp);
    this.router.post("/recharge", verifyToken, this.recharge);
    this.router.post("/recharge/evertec", verifyToken, this.rechargeEvertec);
    this.router.post("/transactions", verifyToken, this.getMovements);
    this.router.get("/list", verifyToken, this.getInfoProductosOldVersion);
    this.router.post("/topfive", verifyToken, this.getTopFive);
    this.router.post("/pin", verifyToken, this.processPin);
    this.router.post("/activation", verifyToken, this.processPin);
    this.router.get("/getcountry", this.getCountry);
    this.router.post("/verifypass", verifyToken, this.verifyPass);
    this.router.post("/crealink", verifyToken, this.creaRedesLink);
    this.router.post("/giftcard", verifyToken, this.giftcardProcess);
    this.router.post("/save-merchantfee", verifyToken, this.saveMerchantfee);

    //CSQ OLD integration
    this.router.get("/validatedoc/:doc", verifyToken, this.validateDoc);
    this.router.get("/validatephone/:num", verifyToken, this.validatePhone);
    this.router.post("/register-sm", verifyToken, this.registerSM);
    this.router.post("/sale-sm", verifyToken, this.saleSM);


    this.router.get("/info-link/:codelink", this.getInfoLink);

    //MS REDES LINK
    this.router.post("/ms-crealink", verifyToken, this.creaRedesLinkMs);

  }
}

const productsController = new ProductsController();
export default productsController.router;
