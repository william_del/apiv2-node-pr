import { Router, Request, Response } from "express";
import { ILogin } from "../models/interfaces/ILogin";
import userService from "../service/userService.service";
import HttpStatusCode from "../util/HttpStatus";
import { verifyToken } from "../middlewares/verifyToken";
import { sendEmail } from "../util/globalFunctions";
import { verifyRecaptcha } from "../middlewares/verifyCaptcha";

class UserController {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  async login(req: Request, res: Response) {
    const user: ILogin = req.body;
    const { status, message, data } = await userService.login(user);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async loginVerification(req: Request, res: Response) {
    const token = req.body.payload;
    const { ippub, ippriv } = req.body;
    const { status, message, data } = await userService.loginVerification(token, ippub, ippriv);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async confirmCodVerification(req: Request, res: Response) {
    const token = req.body.payload;
    const { code, ippub, ippriv } = req.body;
    const { status, message, data } = await userService.confirmCodVerification(token, code, ippub, ippriv);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async getInfo(req: Request, res: Response) {
    const token = req.body.payload;

    const { status, message, data } = await userService.infoUser(
      token
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async registerUser(req: Request, res: Response) {
    const { nombre, apellido, indicativoPais, celular, email, clave } = req.body;
    const { status, message, data } = await userService.createComercio(nombre, apellido, celular, email, clave);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async preRegisterUser(req: Request, res: Response) {
    const { remoteIp, recaptcha } = req.body;
    const isVerified = await verifyRecaptcha(recaptcha, remoteIp);
    if (isVerified) {
      const { firstname, lastname, cellphone, email, nomComercial, nomLegal, address, country } = req.body;
      const { status, message, data } = await userService.createPreRegister(firstname, lastname, cellphone, email, nomComercial, nomLegal, address, country);
      status
        ? res.status(200).json({ status, message, data })
        : res.status(202).json({ status, message });
  
      //await sendEmail('wiilliiam.delgado@gmail.com', 'Prueba', '<h1> Prueba Mandrill Node </h1>');
      //res.status(200).json({ status: true, message: 'Email enviado correctamente' });
    } else {
      res.status(202).json({ status: false, message: 'Error al enviar el email' });
    }
  }

  async findCajero(req: Request, res: Response) {
    const user: ILogin = req.body;
    const { status, message, data } = await userService.findCajero(user);
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async changePass(req: Request, res: Response) {
    const token = req.body.payload;
    const { oldPass, newPass, confirmPass } = req.body;

    const { status, message, data } = await userService.changePass(
      token,
      oldPass,
      newPass,
      confirmPass
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async allUserPdvs(req: Request, res: Response) {
    const token = req.body.payload;

    const { status, message, data } = await userService.allUserPdvs(
      token
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async inactiveUserPdv(req: Request, res: Response) {
    const userid = req.params.userid;
    const estado = req.params.estado;

    const { status, message, data } = await userService.inactiveUserPdv(
      userid, estado
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async restoreSession(req: Request, res: Response) {
    const { msg, phone, comeId } = req.body;
    await userService.restoreSession(comeId, msg, phone);
    res.status(200).json({ status: true, message: 'Msg enviado correctamente' });
  }

  async generateSession(req: Request, res: Response) {
    const { comeId } = req.body;
    const existQR = await userService.generateSession(comeId);
    //console.log("🚀 ~ UserController ~ generateSession ~ existQR:", existQR)
    res.status(200).json({ status: true, qrCodeUrl: existQR });
  }

  routes() {
    this.router.post("/restore-session", this.restoreSession);
    this.router.post("/generate-session", this.generateSession);
    this.router.post("/login", this.login);
    this.router.post("/login-verification", verifyToken, this.loginVerification);
    this.router.post("/confirm-codverification", verifyToken, this.confirmCodVerification);
    this.router.get("/info-user", verifyToken, this.getInfo);
    this.router.post("/pre-register", this.preRegisterUser);
    this.router.post("/register", this.registerUser);
    this.router.post("/findsupercajero", this.findCajero);
    this.router.post("/changepassword", verifyToken, this.changePass);
    this.router.get("/all-user-pdvs", verifyToken, this.allUserPdvs);
    this.router.put("/inactive-user-pdv/:userid/:estado", verifyToken, this.inactiveUserPdv);
  }
}

const userController = new UserController();
export default userController.router;
