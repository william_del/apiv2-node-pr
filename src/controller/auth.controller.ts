import { Router, Request, Response } from "express";
import blacklistService from "../util/auth/blackList";
const crypto = require('crypto');
import moment from 'moment';

const algorithm = 'aes-256-cbc';
const keyString = 'vObbGa4xZ5kQxqessvyOBf0EPGGIaRi+';
const key = crypto.createHash('sha256').update(String(keyString)).digest('base64').substr(0, 32);



class AuthController {
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    async func(req: Request, res: Response) {
        const clientIp = req.ip;  // Obtener la IP del cliente
        console.log("Client IP:", clientIp);

        let ipTemp = '127.0.0.1'
        if (blacklistService.isBlacklisted(ipTemp)) {
            res.status(202).json({ message: 'IP blocked due to suspicious activity' });
        }

        console.log("🚀 ~ key:", key)
        const hash = req.headers.hash;
        console.log("🚀 ~ AuthController ~ func ~ hash:", hash)
        const timestamp = Date.now().toString();
        console.log("🚀 ~ AuthController ~ func ~ timestamp:", timestamp)
        const encryptedTimestamp = encryptString(timestamp);
        console.log("🚀 ~ AuthController ~ func ~ encryptedTimestamp:", encryptedTimestamp)
        const decryptedTimestamp = decryptString(hash);
        console.log("🚀 ~ AuthController ~ func ~ decryptedTimestamp:", decryptedTimestamp)
        
        const date = moment(decryptedTimestamp);
        console.log("🚀 ~ AuthController ~ func ~ date:", date)

    }

    routes() {
        this.router.post("/func", this.func);
    }
}

function encryptString(text) {
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return iv.toString('hex') + '-' + encrypted;
}

function decryptString(text) {
    try {
        let textParts = text.split('-');
        let iv = Buffer.from(textParts.shift(), 'hex');
        let encryptedText = Buffer.from(textParts.join('-'), 'hex');
        let decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
        let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;
    } catch (error) {
        console.log("🚀 ~ decryptString ~ error:", error)
    }
    
}

const authController = new AuthController();
export default authController.router;

