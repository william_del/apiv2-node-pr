import { Router, Request, Response } from "express";
import { IBalance } from "../models/interfaces/IBalance";
import { verifyToken } from "../middlewares/verifyToken";
import balanceService from "../service/balance.service";

class BalanceController {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  async rechargeBalance(req: Request, res: Response) {
    const token = req.body.payload;
    const infoRequest: IBalance = req.body;

    const { status, message, data } = await balanceService.rechargeBalance(
      token,
      infoRequest
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async rechargeBalancePreview(req: Request, res: Response) {
    const token = req.body.payload;
    const { come, valor } = req.body;

    const { status, message, data } = await balanceService.rechargeBalancePreview(
      come, valor
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  /*async rechargeBalancePreviewTest(req: Request, res: Response) {
    const token = req.body.payload;
    const { come, valor } = req.body;

    const { status, message, data } = await balanceService.rechargeBalancePreviewTest(
      token
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }*/

  async transferBalance(req: Request, res: Response) {
    const token = req.body.payload;
    const infoRequest: IBalance = req.body;

    const { status, message, data } = await balanceService.transferBalance(
      token,
      infoRequest
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  async webhookAthm(req: Request, res: Response) {

    console.log(req.body);
    //console.log(req.body.dailyTransactionID);
    //console.log(req.body.metadata1);

    const uniqueId = req.body.metadata1;
    const trxIdAth = req.body.referenceNumber;
    const statusAth = req.body.status;
    const valueAth = req.body.total;

    console.log("uniqueId ",uniqueId ,"trxIdAth ",trxIdAth ,"statusAth ",statusAth ,"valueAth ",valueAth );

    const { status, message, data } = await balanceService.webhookAthm(
      uniqueId,
      trxIdAth,
      statusAth,
      valueAth
    );
    status
      ? res.status(200).json({ status, message, data })
      : res.status(202).json({ status, message });
  }

  routes() {
    this.router.post("/recharge-balance-preview", verifyToken, this.rechargeBalancePreview);
    //this.router.post("/recharge-balance-previewtest", verifyToken, this.rechargeBalancePreviewTest);
    this.router.post("/recharge-balance", verifyToken, this.rechargeBalance);

    this.router.post("/transfer-balance", verifyToken, this.transferBalance);
    this.router.post("/webhook-athmovil", this.webhookAthm);
  }
}

const balanceController = new BalanceController();
export default balanceController.router;
